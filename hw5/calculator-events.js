const EventEmitter = require('events');
const Calculator = require(`./calculator`).Calculator;

const calculatorEvents = new EventEmitter();
const newCalculator = new Calculator();

calculatorEvents.on('divide', (args) => newCalculator.divide(args));
calculatorEvents.on('multiply', (args) => newCalculator.multiply(args));
calculatorEvents.on('sum', (args) => newCalculator.sum(args));
calculatorEvents.on('deduct', (args) => newCalculator.deduct(args));
calculatorEvents.on('remainer', (args) => newCalculator.remainer(args));

calculatorEvents.on('result', () => console.log(`The result is: ${newCalculator.result}.\n`));

module.exports = {
    calculatorEvents,
    newCalculator
}