@navigation
Feature: Github Home Page Navigation
    
    Background:
        Given open web page https://github.com/

    @textVerify
    Scenario: Verify the links inner text
        When scroll to navigation links
        Then the text of the link <Index> contains <Text>

        Examples:
            | Text        | Index |
            | Code        | 1     |
            | Collaborate | 2     |
            | Develop     | 3     |
            | Automate    | 4     |
            | Secure      | 5     |
            | Community   | 6     |

    @redirection
    Scenario: Redirect to sign-in page
        When scroll to header
        And click on Sign In button
        Then the user is redirected to sign-in page