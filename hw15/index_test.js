const assert = require(`assert`);
const expect = require(`chai`).expect;
const fetch = require(`node-fetch`);

const Request = require(`./request`);
const testRequest = new Request();
const baseUrl = `https://jsonplaceholder.typicode.com`;

describe('API GET requests for JSON placeholder data', function () {
    it(`Should return response to valid requests for posts`, async () => {
        const urlPosts = `${baseUrl}/posts`;
        const status = await testRequest.getResponseStatus(urlPosts, `GET`);
        const json = await testRequest.getResponseInJson(urlPosts, `GET`);

        assert.ok(status);
        expect(json.length).to.be.deep.equal(100);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(4);
            expect(Object.keys(item).join(` `)).to.be.deep.equal(`userId id title body`);
        });
        let users = json.map((item) => item.userId);
        let filteredUsers = users.filter((item, index) => {
            return users.indexOf(item) === index;
        });
        expect(filteredUsers.length).to.be.deep.equal(10);
    });

    it(`Should return response to valid requests for comments`, async () => {
        const urlComments = `${baseUrl}/comments`;
        const status = await testRequest.getResponseStatus(urlComments, `GET`);
        const json = await testRequest.getResponseInJson(urlComments, `GET`);

        assert.ok(status);
        expect(json.length).to.be.deep.equal(500);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(5);
            expect(Object.keys(item).join(` `)).to.be.deep.equal(`postId id name email body`);
        });
        let comments = json.map((item) => item.postId);
        let filteredComments = comments.filter((item, index) => {
            return comments.indexOf(item) === index;
        });
        expect(filteredComments.length).to.be.deep.equal(100);
    });

    it(`Should return response to valid requests for albums`, async () => {
        const urlAlbums = `${baseUrl}/albums`;
        const status = await testRequest.getResponseStatus(urlAlbums, `GET`);
        const json = await testRequest.getResponseInJson(urlAlbums, `GET`);

        assert.ok(status);
        expect(json.length).to.be.deep.equal(100);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(3);
            expect(Object.keys(item).join(` `)).to.be.deep.equal(`userId id title`);
        });
        let users = json.map((item) => item.userId);
        let filteredUsers = users.filter((item, index) => {
            return users.indexOf(item) === index;
        });
        expect(filteredUsers.length).to.be.deep.equal(10);
    });

    it(`Should return response to valid requests for photos`, async () => {
        const urlPhotos = `${baseUrl}/photos`;
        const status = await testRequest.getResponseStatus(urlPhotos, `GET`);
        const json = await testRequest.getResponseInJson(urlPhotos, `GET`);

        assert.ok(status);
        expect(json.length).to.be.deep.equal(5000);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(5);
            expect(Object.keys(item).join(` `)).to.be.deep.equal(`albumId id title url thumbnailUrl`);
        });
        let albums = json.map((item) => item.albumId);
        let filteredAlbums = albums.filter((item, index) => {
            return albums.indexOf(item) === index;
        });
        expect(filteredAlbums.length).to.be.deep.equal(100);
    });

    it(`Should return response to valid requests for todos`, async () => {
        const urlTodos = `${baseUrl}/todos`;
        const status = await testRequest.getResponseStatus(urlTodos, `GET`);
        const json = await testRequest.getResponseInJson(urlTodos, `GET`);

        assert.ok(status);
        expect(json.length).to.be.deep.equal(200);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(4);
            expect(Object.keys(item).join(` `)).to.be.deep.equal(`userId id title completed`);
        });
        let users = json.map((item) => item.userId);
        let filteredUsers = users.filter((item, index) => {
            return users.indexOf(item) === index;
        });
        expect(filteredUsers.length).to.be.deep.equal(10);
    });

    it(`Should return response to valid requests for users`, async () => {
        const urlUsers = `${baseUrl}/users`;
        const status = await testRequest.getResponseStatus(urlUsers, `GET`);
        const json = await testRequest.getResponseInJson(urlUsers, `GET`);

        assert.ok(status);
        expect(json.length).to.be.deep.equal(10);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(8);
            expect(Object.keys(item).join(` `)).to.be.deep.equal(`id name username email address phone website company`);
            expect(Object.keys(item.address).length).to.be.deep.equal(5);
            expect(Object.keys(item.address).join(` `)).to.be.deep.equal(`street suite city zipcode geo`);
            expect(Object.keys(item.address.geo).length).to.be.deep.equal(2);
            expect(Object.keys(item.address.geo).join(` `)).to.be.deep.equal(`lat lng`);
            expect(Object.keys(item.company).length).to.be.deep.equal(3);
            expect(Object.keys(item.company).join(` `)).to.be.deep.equal(`name catchPhrase bs`);
        });
    });

    it(`Should return response to valid requests for nested resources`, async () => {
        const urlUsersAlbums = `${baseUrl}/users/4/albums`;
        const urlPostsComments = `${baseUrl}/posts/50/comments`;
        const urlAlbumsPhotos = `${baseUrl}/albums/33/photos`;

        const statusUsersAlbums = await testRequest.getResponseStatus(urlUsersAlbums, `GET`);
        const statusPostsComments = await testRequest.getResponseStatus(urlPostsComments, `GET`);
        const statusAlbumsPhotos = await testRequest.getResponseStatus(urlAlbumsPhotos, `GET`);

        assert.ok(statusUsersAlbums);
        assert.ok(statusPostsComments);
        assert.ok(statusAlbumsPhotos);

        const jsonUsersAlbums = await testRequest.getResponseInJson(urlUsersAlbums, `GET`);
        const jsonPostsComments = await testRequest.getResponseInJson(urlPostsComments, `GET`);
        const jsonAlbumsPhotos = await testRequest.getResponseInJson(urlAlbumsPhotos, `GET`);

        jsonUsersAlbums.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(3);
            expect(Object.keys(item).join(` `)).to.be.deep.equal(`userId id title`);
        });

        jsonPostsComments.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(5);
            expect(Object.keys(item).join(` `)).to.be.deep.equal(`postId id name email body`);
        });

        jsonAlbumsPhotos.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(5);
            expect(Object.keys(item).join(` `)).to.be.deep.equal(`albumId id title url thumbnailUrl`);
        });
    });

    it(`Should return response to valid requests for specified post`, async () => {
        const urlPost = `${baseUrl}/posts/23`;
        const status = await testRequest.getResponseStatus(urlPost, `GET`);
        const json = await testRequest.getResponseInJson(urlPost, `GET`);

        assert.ok(status);
        expect(Object.keys(json).length).to.be.deep.equal(4);
        expect(json.title).to.be.deep.equal(`maxime id vitae nihil numquam`);
    });

    it(`Should return response to valid requests for specified comment`, async () => {
        const urlComment = `${baseUrl}/comments?id=3`;
        const status = await testRequest.getResponseStatus(urlComment, `GET`);
        const json = await testRequest.getResponseInJson(urlComment, `GET`);

        assert.ok(status);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(5);
            expect(item.email).to.be.deep.equal(`Nikita@garfield.biz`);
        });
    });

    it(`Should return response to valid requests for specified album`, async () => {
        const urlAlbum = `${baseUrl}/users/5/albums?id=48`;
        const status = await testRequest.getResponseStatus(urlAlbum, `GET`);
        const json = await testRequest.getResponseInJson(urlAlbum, `GET`);

        assert.ok(status);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(3);
            expect(item.title).to.be.deep.equal(`modi consequatur culpa aut quam soluta alias perspiciatis laudantium`);
        });
    });

    it(`Should return response to valid requests for specified photo`, async () => {
        const urlPhoto = `${baseUrl}/photos/3452`;
        const status = await testRequest.getResponseStatus(urlPhoto, `GET`);
        const json = await testRequest.getResponseInJson(urlPhoto, `GET`);

        assert.ok(status);
        expect(Object.keys(json).length).to.be.deep.equal(5);
        expect(json.url).to.be.deep.equal(`https://via.placeholder.com/600/41b07a`);
    });

    it(`Should return response to valid requests for specified todo`, async () => {
        const urlTodo = `${baseUrl}/users/9/todos?title=recusandae quia qui sunt libero`;
        const status = await testRequest.getResponseStatus(urlTodo, `GET`);
        const json = await testRequest.getResponseInJson(urlTodo, `GET`);

        assert.ok(status);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(4);
            expect(item.id).to.be.deep.equal(168);
        });
    });

    it(`Should return response to valid requests for specified user`, async () => {
        const urlUsers = `${baseUrl}/users?username=Antonette`;
        const status = await testRequest.getResponseStatus(urlUsers, `GET`);
        const json = await testRequest.getResponseInJson(urlUsers, `GET`);

        assert.ok(status);
        json.forEach(item => {
            expect(Object.keys(item).length).to.be.deep.equal(8);
            expect(item.name).to.be.deep.equal(`Ervin Howell`);
        });
    });
});

describe('API POST requests for JSON placeholder data', function () {
    it(`Should return response to valid requests for a new post`, async () => {
        const urlPosts = `${baseUrl}/posts`;
        const bodyItem = {
            userId: 10,
            id: 101,
            title: `The title of the new post`,
            body: `The text of the new post comes here.`,
            testPost: `testPostValue`
        };
        const status = await testRequest.getResponseStatus(urlPosts, `POST`, bodyItem);
        const json = await testRequest.getResponseInJson(urlPosts, `POST`, bodyItem);

        assert.ok(status);
        expect(Object.keys(json).length).to.be.deep.equal(5);
        expect(Object.keys(json).join(` `)).to.be.deep.equal(`userId id title body testPost`);
    });

    it(`Should return response to valid requests for a new comment`, async () => {
        const urlComments = `${baseUrl}/comments`;
        const bodyItem = {
            postId: 55,
            id: 501,
            name: `The name of the new post`,
            email: `vladikh2405@gmail.com`,
            body: `The text of the new comment comes here.`,
            testPost: `testPostValue`,
            moreTestPost: `moreTestPostValue`
        };
        const status = await testRequest.getResponseStatus(urlComments, `POST`, bodyItem);
        const json = await testRequest.getResponseInJson(urlComments, `POST`, bodyItem);

        assert.ok(status);
        expect(Object.keys(json).length).to.be.deep.equal(7);
        expect(Object.keys(json).join(` `)).to.be.deep.equal(`postId id name email body testPost moreTestPost`);
    });

    it(`Should return response to valid requests for a new album`, async () => {
        const urlAlbums = `${baseUrl}/albums`;
        const bodyItem = {
            userId: 10,
            id: 104,
            title: `The title of the new album`
        };
        const status = await testRequest.getResponseStatus(urlAlbums, `POST`, bodyItem);
        const json = await testRequest.getResponseInJson(urlAlbums, `POST`, bodyItem);

        assert.ok(status);
        expect(Object.keys(json).length).to.be.deep.equal(3);
        expect(Object.keys(json).join(` `)).to.be.deep.equal(`userId id title`);
    });

    it(`Should return response to valid requests for a new photo`, async () => {
        const urlPhotos = `${baseUrl}/photos`;
        const bodyItem = {
            albumId: 10,
            id: 5008,
            title: `My photo`,
            url: `https://vk.com/id205332073?z=photo205332073`,
            thumbnailUrl: `https://vk.com/id205332073?z=photo205332073`
        };
        const status = await testRequest.getResponseStatus(urlPhotos, `POST`, bodyItem);
        const json = await testRequest.getResponseInJson(urlPhotos, `POST`, bodyItem);

        assert.ok(status);
        expect(Object.keys(json).length).to.be.deep.equal(5);
        expect(Object.keys(json).join(` `)).to.be.deep.equal(`albumId id title url thumbnailUrl`);
    });

    it(`Should return response to valid requests for a new todo`, async () => {
        const urlTodos = `${baseUrl}/todos`;
        const bodyItem = {
            userId: 10,
            id: 201,
            title: `Learn HTTP Requests`,
            completed: true
        };
        const status = await testRequest.getResponseStatus(urlTodos, `POST`, bodyItem);
        const json = await testRequest.getResponseInJson(urlTodos, `POST`, bodyItem);

        assert.ok(status);
        expect(Object.keys(json).length).to.be.deep.equal(4);
        expect(Object.keys(json).join(` `)).to.be.deep.equal(`userId id title completed`);
    });

    it(`Should return response to valid requests for a new user`, async () => {
        const urlUsers = `${baseUrl}/users`;
        const bodyItem = {
            id: 10,
            name: 201,
            username: `Learn HTTP Requests`,
            email: true,
            address: {
                street: "Victory St.",
                suite: "Apt. 256",
                city: "Minsk",
                zipcode: "22020",
                geo: {
                    lat: "-37.3159",
                    lng: "81.1496"
                }
            },
            phone: "+375445783855",
            website: "epam.com",
            company: "EPAM"
        };
        const status = await testRequest.getResponseStatus(urlUsers, `POST`, bodyItem);
        const json = await testRequest.getResponseInJson(urlUsers, `POST`, bodyItem);

        assert.ok(status);
        expect(Object.keys(json).length).to.be.deep.equal(8);
        expect(Object.keys(json).join(` `)).to.be.deep.equal(`id name username email address phone website company`);
    });
});

describe('Invalid requests for JSON placeholder data', function () {
    it(`Should return failed response status to invalid GET requests`, async () => {
        const urlInvalid1 = `${baseUrl}/music`;
        const urlInvalid2 = `${baseUrl}/albums/photos`;
        const urlInvalid3 = `${baseUrl}/user`;

        const statusInvalid1 = await testRequest.getResponseStatus(urlInvalid1, `GET`);
        const statusInvalid2 = await testRequest.getResponseStatus(urlInvalid2, `GET`);
        const statusInvalid3 = await testRequest.getResponseStatus(urlInvalid3, `GET`);

        const statusTextInvalid1 = await testRequest.getResponseStatusText(urlInvalid1, `GET`);
        const statusTextInvalid2 = await testRequest.getResponseStatusText(urlInvalid2, `GET`);
        const statusTextInvalid3 = await testRequest.getResponseStatusText(urlInvalid3, `GET`);

        assert.ok(!statusInvalid1);
        assert.ok(!statusInvalid2);
        assert.ok(!statusInvalid3);

        expect(statusTextInvalid1).to.be.deep.equal(`Not Found`);
        expect(statusTextInvalid2).to.be.deep.equal(`Not Found`);
        expect(statusTextInvalid3).to.be.deep.equal(`Not Found`);
    });

    it(`Should return failed response status to invalid POST requests`, async () => {
        const urlInvalid1 = `${baseUrl}/music`;
        const urlInvalid2 = `${baseUrl}/albums/photos`;
        const urlInvalid3 = `${baseUrl}/user`;

        const bodyItem = {
            itemId: 55,
            name: `The name of the new item`,
            body: `The text of the new item comes here.`,
            testPost: `testPostValue`,
        };

        const responseInvalid1 = await testRequest.performRequest(urlInvalid1, `POST`, bodyItem);
        const responseInvalid2 = await testRequest.performRequest(urlInvalid2, `POST`, bodyItem);
        const responseInvalid3 = await testRequest.performRequest(urlInvalid3, `POST`, bodyItem);

        assert.ok(!responseInvalid1.ok);
        assert.ok(!responseInvalid2.ok);
        assert.ok(!responseInvalid3.ok);

        expect(responseInvalid1.statusText).to.be.deep.equal(`Not Found`);
        expect(responseInvalid2.statusText).to.be.deep.equal(`Not Found`);
        expect(responseInvalid3.statusText).to.be.deep.equal(`Not Found`);
    });
})