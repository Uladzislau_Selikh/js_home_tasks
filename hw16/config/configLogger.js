const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

// eslint-disable-next-line no-shadow
const myFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});
const timeFormat = `YYYY-MM-DD HH:mm:ss`;

const logger = createLogger({
    format: combine(
        label({ label: 'LOG' }),
        timestamp({
            format: timeFormat
        }),
        myFormat
    ),
    transports: [
        new transports.Console({
            level: `debug`,
            eol: '\r\n',
            colorize: true,
        }),
        new transports.File({
            dirname: './hw16/logs',
            filename: `gitHubTest.log`,
            level: 'debug',
            options: { flags: 'w' }
        }),
    ]
});

module.exports = {
    logger
}