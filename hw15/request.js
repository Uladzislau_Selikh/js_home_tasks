const fetch = require(`node-fetch`);

module.exports = class Request {
    constructor() { };
    async performRequest(url, methodType, bodyItem) {
        switch (methodType) {
            case `GET`:
                return await fetch(url, {
                    method: methodType
                });
            default:
                return await fetch(url, {
                    method: methodType,
                    body: JSON.stringify(bodyItem),
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8',
                    }
                })
        }
    }

    async getResponseStatus(url, methodType, bodyItem) {
        return this.performRequest(url, methodType, bodyItem)
            .then(response => response.ok);
    }

    async getResponseStatusText(url, methodType, bodyItem) {
        return this.performRequest(url, methodType, bodyItem)
            .then(response => response.statusText);
    }

    async getResponseInJson(url, methodType, bodyItem) {
        return this.performRequest(url, methodType, bodyItem)
            .then(response => response.json());
    };
}