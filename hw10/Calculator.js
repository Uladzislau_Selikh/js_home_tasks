class Calculator {

    constructor() {
        this.result = 0;
        this.errorMessage = ``;
    }

    reset() {
        this.result = 0;
        this.errorMessage = ``;
        return this;
    }

    returnResult() {
        try {
        if(!this.errorMessage) return (this.result === -0)? 0 : this.result;
        else throw new Error();
        } catch(error) {
            let lastValue = this.result;
            let lastErrorMessage = this.errorMessage;
            this.reset();
            return `${lastErrorMessage} The error occured when the result was ${lastValue}!`;
        }
    }

    add(...items) {
        if (this.errorMessage) return this;
        try {
        items.forEach((item) => {
            if (typeof item === `number` && !Number.isNaN(item)) {
                if (this.result + item <= Number.MAX_SAFE_INTEGER && this.result + item >= Number.MIN_SAFE_INTEGER) {
                    if ((Math.floor(item) === item && Math.floor(this.result) === this.result)) this.result += item;
                    else this.result = +(this.result + item).toFixed(11);
                } else throw new Error(`The number is too big or too small!`);
            }
            else throw new Error(`The argument passed is not a number!`);
        })
        return this;
    } catch(error) {
        this.errorMessage = error.message;
        return this;
    }
    }

    multiply(...items) {
        if (this.errorMessage) return this;
        try {
        items.forEach((item) => {
            if (typeof item === `number` && !Number.isNaN(item)) {
                if (this.result * item <= Number.MAX_SAFE_INTEGER && this.result * item >= Number.MIN_SAFE_INTEGER) {
                    if ((Math.floor(item) === item && Math.floor(this.result) === this.result)) this.result = ((this.result === -0)? 0 : this.result) * ((item === -0)? 0 : item);
                    else this.result = +(this.result * item).toFixed(11);
                } else throw new Error(`The number is too big or too small!`);
            }
            else throw new Error(`The argument passed is not a number!`);
        })
        return this;
    } catch(error) {
        this.errorMessage = error.message;
        return this;
    }
    }

    deduct(...items) {
        if (this.errorMessage) return this;
        try {
        items.forEach((item) => {
            if (typeof item === `number` && !Number.isNaN(item)) {
                if (this.result - item <= Number.MAX_SAFE_INTEGER && this.result - item >= Number.MIN_SAFE_INTEGER) {
                    if ((Math.floor(item) === item && Math.floor(this.result) === this.result)) this.result -= item;
                    else this.result = +(this.result - item).toFixed(11);
                } else throw new Error(`The number is too big or too small!`);
            }
            else throw new Error(`The argument passed is not a number!`);
        })
        return this;
    } catch(error) {
        this.errorMessage = error.message;
        return this;
    }
    }

    divide(...items) {
        if (this.errorMessage) return this;
        try {
        items.forEach((item) => {
            if (typeof item === `number` && !Number.isNaN(item)) {
                if (this.result / item <= Number.MAX_SAFE_INTEGER && this.result / item >= Number.MIN_SAFE_INTEGER) {
                        if (+(this.result / item).toFixed(11) === 0 && this.result !== 0 && item !== 0) throw new Error(`Too many zero digit places in the result!`);
                        else if (!+item.toFixed(11)) throw new Error(`Too many zero digit places in the argument!`);
                        else this.result = +(this.result / item).toFixed(11)
                }
                else if (Number.isNaN(this.result / item)) throw new Error(`You can not divide zero by zero!`)
                else throw new Error(`The number is too big or too small!`)
            }
            else throw new Error(`The argument passed is not a number!`);
        })
        return this;
    } catch(error) {
        this.errorMessage = error.message;
        return this;
    }
    }
}

module.exports = {
    Calculator
}

const newCalculator = new Calculator();