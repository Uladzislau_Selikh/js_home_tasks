const { browser } = require("protractor");
const { After, setDefaultTimeout, Status} = require(`@cucumber/cucumber`);

setDefaultTimeout(240 * 1000);

// eslint-disable-next-line new-cap
After(async function(testCase) {
    if (testCase.result.status === Status.FAILED) {
    let imageData = await browser.takeScreenshot();
    // eslint-disable-next-line no-invalid-this
    await this.attach(Buffer.from(imageData, 'base64'), 'image/png');
    }
})
