const Calculator = require("./Calculator").Calculator;
const newCalculator = new Calculator();
const assert = require('assert');

describe('Calculator Addition Function', function () {

    beforeEach(() => {
        newCalculator.reset();
    });

    it('should correctly display default result', function () {
        assert.strictEqual(newCalculator.returnResult(), 0);
    });

    it('should correctly add number to the default result', function () {
        assert.strictEqual(newCalculator.add(4).returnResult(), 4);
        assert.strictEqual(newCalculator.reset().add(10).returnResult(), 10);
        assert.strictEqual(newCalculator.reset().add(-34).returnResult(), -34);
    });

    it('should correctly add numbers after Calculator reset', function () {
        assert.strictEqual(newCalculator.add(4, 5).returnResult(), 9);
        assert.strictEqual(newCalculator.reset().add(35, 2).returnResult(), 37);
        assert.strictEqual(newCalculator.reset().add(10, (3 + 2), 21, 99).returnResult(), 135);
        assert.strictEqual(newCalculator.reset().add(2).add(5 * 11).add(36).returnResult(), 93);
        assert.strictEqual(newCalculator.reset().add(-5, 34).returnResult(), 29);
    });

    it('should correctly add numbers without Calculator reset', function () {
        assert.strictEqual(newCalculator.add(4, 5).returnResult(), 9);
        assert.strictEqual(newCalculator.add(10, 6 / 2, -0).returnResult(), 22);
        assert.strictEqual(newCalculator.add(56 + 44, -34).returnResult(), 88);
        assert.strictEqual(newCalculator.add(-88).returnResult(), 0);
        assert.strictEqual(newCalculator.add(4, 5, 45).returnResult(), 54);
        assert.strictEqual(newCalculator.add(-5).add(77).returnResult(), 126);
        assert.strictEqual(newCalculator.add(-200).add(-12, 92, -10).returnResult(), -4);
    });

    it('should correctly add numbers with floating point', function () {
        assert.strictEqual(newCalculator.add(1).add(11.55).add(-0.33).returnResult(), 12.22);
        assert.strictEqual(newCalculator.reset().add(67.444).add(3.566, 90).returnResult(), 161.01);
        assert.strictEqual(newCalculator.add(0.99).returnResult(), 162);
        assert.strictEqual(newCalculator.reset().add(0.08, 1.11000).returnResult(), 1.19);
        assert.strictEqual(newCalculator.reset().add(1.3453453488881).add(119.344, 0.0001).returnResult(), 120.68944534889);
        assert.strictEqual(newCalculator.add(-22.68944534859).add(2).returnResult(), 100.0000000003);
        assert.strictEqual(newCalculator.reset().add(138.689445348596).add(2.454577777184).returnResult(), 141.14402312578);
    });

    it('should correctly handle huge and very small numbers', function () {
        assert.strictEqual(newCalculator.add(9007199254740992).add(-1).returnResult(), `The number is too big or too small! The error occured when the result was 0!`);
        assert.strictEqual(newCalculator.add(9007199254740991, 34).add(-35).returnResult(), `The number is too big or too small! The error occured when the result was 9007199254740991!`);
        assert.strictEqual(newCalculator.add(9007199254741000, -10).add(-2).returnResult(), `The number is too big or too small! The error occured when the result was 0!`);
        assert.strictEqual(newCalculator.add(-9007199254740991).add(-5).returnResult(), `The number is too big or too small! The error occured when the result was -9007199254740991!`);
        assert.strictEqual(newCalculator.add(40).add(Infinity).add(9007199254740991).returnResult(), `The number is too big or too small! The error occured when the result was 40!`);
        assert.strictEqual(newCalculator.add(Infinity).add(-Infinity).returnResult(), `The number is too big or too small! The error occured when the result was 0!`);
    });

    it('should correctly handle arguments that are not numbers', function () {
        assert.strictEqual(newCalculator.add(1, 5, 45).add(true).returnResult(), `The argument passed is not a number! The error occured when the result was 51!`);
        assert.strictEqual(newCalculator.add(15, undefined, 2).add(44, 78).returnResult(), `The argument passed is not a number! The error occured when the result was 15!`);
        assert.strictEqual(newCalculator.add(16, 2).add( { number: 4 }, 33 ).returnResult(), `The argument passed is not a number! The error occured when the result was 18!`);
        assert.strictEqual(newCalculator.add(false).reset().add(68).add(1, `90`).returnResult(), `The argument passed is not a number! The error occured when the result was 69!`);
        assert.strictEqual(newCalculator.add(56, [55, 890]).add(132).returnResult(), `The argument passed is not a number! The error occured when the result was 56!`);
    });

});

describe('Calculator Deduction Function', function () {

    beforeEach(() => {
        newCalculator.reset();
    });

    it('should correctly deduct number from the default result', function () {
        assert.strictEqual(newCalculator.deduct(15).returnResult(), -15);
        assert.strictEqual(newCalculator.reset().deduct(31).returnResult(), -31);
        assert.strictEqual(newCalculator.deduct(-34).returnResult(), 3);
    });

    it('should correctly deduct numbers after Calculator reset', function () {
        assert.strictEqual(newCalculator.deduct(4, 9).returnResult(), -13);
        assert.strictEqual(newCalculator.reset().deduct(24 * 2, 2, -0, 9).returnResult(), -59);
        assert.strictEqual(newCalculator.reset().deduct(-83, - 16 - 18).returnResult(), 117);
        assert.strictEqual(newCalculator.reset().deduct(56, -5).deduct(-90).returnResult(), 39);
        assert.strictEqual(newCalculator.reset().deduct(-5, 34).deduct(119).returnResult(), -148);
    });

    it('should correctly deduct numbers without Calculator reset', function () {
        assert.strictEqual(newCalculator.deduct(13, -1).returnResult(), -12);
        assert.strictEqual(newCalculator.deduct(5 * 2, 8, 9).returnResult(), -39);
        assert.strictEqual(newCalculator.deduct(-4).returnResult(), -35);
        assert.strictEqual(newCalculator.deduct(35, -5).deduct(-90 / 2).deduct().returnResult(), -20);
    });

    it('should correctly deduct numbers with floating point', function () {
        assert.strictEqual(newCalculator.deduct(-13).deduct(11.55).deduct(-0.38).returnResult(), 1.83);
        assert.strictEqual(newCalculator.deduct(55.444).deduct(3.56, 88).returnResult(), -145.174);
        assert.strictEqual(newCalculator.deduct(-5.174).returnResult(), -140);
        assert.strictEqual(newCalculator.reset().deduct(-0.12, 43.88000).returnResult(), -43.76);
        assert.strictEqual(newCalculator.reset().deduct(1.8460263860171).deduct(1339.344, 0.3301).returnResult(), -1341.52012638602);
        assert.strictEqual(newCalculator.deduct(-41.520126386).deduct(2).returnResult(), -1302.00000000002);
        assert.strictEqual(newCalculator.reset().deduct(138.68944534859).deduct(2.68457444931).returnResult(), -141.3740197979);
    });

    it('should correctly handle huge and very small numbers', function () {
        assert.strictEqual(newCalculator.deduct(9007199254740992).deduct(-544).returnResult(), `The number is too big or too small! The error occured when the result was 0!`);
        assert.strictEqual(newCalculator.deduct(-9007199254740991, 34).deduct(-99).returnResult(), `The number is too big or too small! The error occured when the result was 9007199254740957!`);
        assert.strictEqual(newCalculator.deduct(9007199254741000, -10).deduct(-7).returnResult(), `The number is too big or too small! The error occured when the result was 0!`);
        assert.strictEqual(newCalculator.deduct(9007199254740991).deduct(5).returnResult(), `The number is too big or too small! The error occured when the result was -9007199254740991!`);
        assert.strictEqual(newCalculator.deduct(-40).deduct(-Infinity).deduct(9007199254740991).returnResult(), `The number is too big or too small! The error occured when the result was 40!`);
        assert.strictEqual(newCalculator.deduct(-Infinity).deduct(Infinity).returnResult(), `The number is too big or too small! The error occured when the result was 0!`);
    });

    it('should correctly handle arguments that are not numbers', function () {
        assert.strictEqual(newCalculator.deduct(1, -5, 45).deduct(true).returnResult(), `The argument passed is not a number! The error occured when the result was -41!`);
        assert.strictEqual(newCalculator.deduct(15.9, undefined, 2).deduct(44, 3).returnResult(), `The argument passed is not a number! The error occured when the result was -15.9!`);
        assert.strictEqual(newCalculator.deduct(90, -2).deduct( { negativeNumber: -4 }, 33 ).returnResult(), `The argument passed is not a number! The error occured when the result was -88!`);
        assert.strictEqual(newCalculator.deduct(false).reset().deduct(10).deduct(3, `90`).returnResult(), `The argument passed is not a number! The error occured when the result was -13!`);
        assert.strictEqual(newCalculator.reset().deduct(66, 10).deduct([34, 5.5]).deduct(10).returnResult(), `The argument passed is not a number! The error occured when the result was -76!`);
    });

});

describe('Calculator Multiply Function', function () {

    beforeEach(() => {
        newCalculator.reset();
    });

    it('should correctly multiply default result and numbers passed', function () {
        assert.strictEqual(newCalculator.multiply(15).returnResult(), 0);
        assert.strictEqual(newCalculator.multiply(-58).returnResult(), 0);
    });

    it('should correctly multiply numbers after Calculator reset', function () {
        assert.strictEqual(newCalculator.add(1).multiply(15, 2).returnResult(), 30);
        assert.strictEqual(newCalculator.reset().add(1).multiply(-5, 2, 0, 3).returnResult(), 0);
        assert.strictEqual(newCalculator.reset().add(3).multiply(22, 2, -1).returnResult(), -132);
        assert.strictEqual(newCalculator.reset().add(1).multiply(-2, 4.5).multiply(0).returnResult(), 0);
    });

    it('should correctly multuply numbers without Calculator reset', function () {
        assert.strictEqual(newCalculator.add(1).multiply(13, -1).returnResult(), -13);
        assert.strictEqual(newCalculator.multiply(5 * 2, -6 / 2).returnResult(), 390);
        assert.strictEqual(newCalculator.multiply(-0).returnResult(), 0);
        assert.strictEqual(newCalculator.add(4).multiply(-5.0).multiply(-50).multiply(0.3).returnResult(), 300);
    });

    it('should correctly multuply numbers with floating point', function () {
        assert.strictEqual(newCalculator.add(1).multiply(11).multiply(-0.1).returnResult(), -1.1);
        assert.strictEqual(newCalculator.multiply(-1).multiply(4.00).returnResult(), 4.4);
        assert.strictEqual(newCalculator.reset().add(2).multiply(0.5, 6, 1.2, -3.5).returnResult(), -25.2);
        assert.strictEqual(newCalculator.reset().add(1).multiply(0.53481, 6, 0.509155, 2.8).returnResult(), 4.57465991724);
        assert.strictEqual(newCalculator.reset().add(1).multiply(13.1333409).multiply(3, 45.555388).returnResult(), 1794.88332130731);
    });

    it('should correctly handle huge and very small numbers', function () {
        assert.strictEqual(newCalculator.add(1).multiply(9007199254740992).returnResult(), `The number is too big or too small! The error occured when the result was 1!`);
        assert.strictEqual(newCalculator.add(0.1).multiply(-9007199254740991, 34).multiply(-99).returnResult(), `The number is too big or too small! The error occured when the result was -900719925474099.1!`);
        assert.strictEqual(newCalculator.add(1).multiply(56789234666644, -1000, -7).returnResult(), `The number is too big or too small! The error occured when the result was 56789234666644!`);
        assert.strictEqual(newCalculator.add(1).multiply(Infinity).multiply(0).returnResult(), `The number is too big or too small! The error occured when the result was 1!`);
        assert.strictEqual(newCalculator.add(1).multiply(-Infinity).multiply(1 / Infinity).returnResult(), `The number is too big or too small! The error occured when the result was 1!`);
    });

    it('should correctly handle arguments that are not numbers', function () {
        assert.strictEqual(newCalculator.add(2).multiply(1, -5, 45).multiply(false).returnResult(), `The argument passed is not a number! The error occured when the result was -450!`);
        assert.strictEqual(newCalculator.add(1).multiply(33.33, 1.5, undefined, 2).multiply(15, 3).returnResult(), `The argument passed is not a number! The error occured when the result was 49.995!`);
        assert.strictEqual(newCalculator.add(1).multiply(-6.55, -2).multiply( { negativeNumber: 4, string: `9` }, 33 ).returnResult(), `The argument passed is not a number! The error occured when the result was 13.1!`);
        assert.strictEqual(newCalculator.add(1).multiply(false).reset().add(0.5).multiply(98).multiply(3, `90`).returnResult(), `The argument passed is not a number! The error occured when the result was 147!`);
    });

});

describe('Calculator Division Function', function () {

    beforeEach(() => {
        newCalculator.reset();
    });

    it('should correctly divide default value by numbers passed', function () {
        assert.strictEqual(newCalculator.divide(15).returnResult(), 0);
        assert.strictEqual(newCalculator.divide(-58).returnResult(), 0);
        assert.strictEqual(newCalculator.divide(0).returnResult(), `You can not divide zero by zero! The error occured when the result was 0!`);
    });

    it('should correctly divide numbers after Calculator reset', function () {
        assert.strictEqual(newCalculator.add(34).divide(2, 17).returnResult(), 1);
        assert.strictEqual(newCalculator.reset().add(1000).divide(-5, 2, -4, 5).returnResult(), 5);
        assert.strictEqual(newCalculator.reset().add(-88).divide(22, 2, -1).returnResult(), 2);
        assert.strictEqual(newCalculator.reset().add(450).divide(-2, 40 / 8).divide(9).returnResult(), -5);
    });

    it('should correctly divide numbers without Calculator reset', function () {
        assert.strictEqual(newCalculator.add(115).divide(5, -1).returnResult(), -23);
        assert.strictEqual(newCalculator.divide(-23, 1 / 20).returnResult(), 20);
        assert.strictEqual(newCalculator.divide(4).returnResult(), 5);
        assert.strictEqual(newCalculator.divide(-5.00).divide(1).divide(- 1 / 80).returnResult(), 80);
    });

    it('should correctly multuply numbers with floating point', function () {
        assert.strictEqual(newCalculator.add(1.5).divide(11).divide(-0.1).returnResult(), -1.3636363636);
        assert.strictEqual(newCalculator.divide(-1.3636363636 * 200).divide(4.00).returnResult(), 0.00125);
        assert.strictEqual(newCalculator.reset().add(6044).divide(0.5, 222.3, 1.2, -3.54444).returnResult(), -12.78456965021);
        assert.strictEqual(newCalculator.reset().add(111.5).divide(55.53481, 6, 0.666332, 2.8).returnResult(), 0.17935340451);
    });

    it('should correctly handle huge and very small numbers', function () {5.28269137207
        assert.strictEqual(newCalculator.add(1).divide(1 / 9007199254741000).returnResult(), `The number is too big or too small! The error occured when the result was 1!`);
        assert.strictEqual(newCalculator.add(5690).divide(-900719925474099).returnResult(), -0.00000000001);
        assert.strictEqual(newCalculator.add(3).divide(3 / 56789234666644).returnResult(), `Too many zero digit places in the argument! The error occured when the result was 2.99999999999!`);
        assert.strictEqual(newCalculator.reset().add(1).divide(456).divide(Infinity).returnResult(), `Too many zero digit places in the result! The error occured when the result was 0.00219298246!`);
        assert.strictEqual(newCalculator.add(34).divide(456).divide(9007199254741000).returnResult(), `Too many zero digit places in the result! The error occured when the result was 0.07456140351!`);
    });

    it('should correctly handle arguments that are not numbers', function () {
        assert.strictEqual(newCalculator.add(2).divide(1, -5, 45).divide(false).returnResult(), `The argument passed is not a number! The error occured when the result was -0.00888888889!`);
        assert.strictEqual(newCalculator.add(34).divide(17, 2, undefined, 2).divide(15, 3).returnResult(), `The argument passed is not a number! The error occured when the result was 1!`);
        assert.strictEqual(newCalculator.add(100).divide(-7.5, -2).divide( { number: 4, string: `9` }, 33 ).returnResult(), `The argument passed is not a number! The error occured when the result was 6.66666666666!`);
        assert.strictEqual(newCalculator.add(1).divide(false).reset().add(0.5).divide(98).divide(3, `90`).returnResult(), `The argument passed is not a number! The error occured when the result was 0.00170068027!`);
    });

});

describe('Calculator Function Mixture', function () {

    before(() => {
        newCalculator.reset();
    });

    it('should correctly mix the calculator functions', function () {
        assert.strictEqual(newCalculator.add(547).deduct(4440).multiply(3.4).divide(15).returnResult(), -882.41333333333);
        assert.strictEqual(newCalculator.divide(-3529.65333333332).add(9).deduct(100).multiply(55).returnResult(), -4991.25);
        assert.strictEqual(newCalculator.reset().deduct(-1009998).divide(9).multiply(0.022228).returnResult(), 2494.470616);
        assert.strictEqual(newCalculator.reset().deduct(456456000).divide(5.000).multiply(0).returnResult(), 0);
        assert.strictEqual(newCalculator.add(10000000).divide(- 1 / 9007199254741000).deduct(0.0000034).returnResult(), `The number is too big or too small! The error occured when the result was 10000000!`);
        assert.strictEqual(newCalculator.multiply(670).add(9007).divide(0.0000034).add({string: `+345`}).returnResult(), `The argument passed is not a number! The error occured when the result was 2649117647.0588236!`);
    });

});