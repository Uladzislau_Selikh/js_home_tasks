class Track {
  constructor(name, time, explicit, album, artist) {
    this.trackName = name;
    this.trackTime = time;
    this.album = album;
    this.artist = artist;
    this.explicit = explicit;
  }

  assignArtist(artist) {
    this.artist = artist;
  }

  assisnAlbum(album) {
    this.album = album;
  }
}

  module.exports = {
      Track
  }