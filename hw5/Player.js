const EventEmitter = require('events');
const player = new EventEmitter();

player.on('play', (arg) => {

    let start = () => {
        if (arg.tracks[arg.currentTrackIndex] && arg.secondsPlayed <= arg.tracks[arg.currentTrackIndex].trackTime && !arg.pauseButton) {

            if ((!arg.nextButton && !arg.previousButton) || (arg.nextButton && arg.previousButton)) {
                console.log(!arg.secondsPlayed ? `${arg.tracks[arg.currentTrackIndex].trackName} is playing...` : arg.secondsPlayed);
                arg.secondsPlayed++;
                setTimeout(() => start(), 1000);
            } else if (arg.nextButton && !arg.previousButton) {
                arg.nextButton = false;
                try {
                    console.log(!arg.secondsPlayed ? `${arg.tracks[arg.currentTrackIndex].trackName} is playing...` : arg.secondsPlayed);

                    if (arg.currentTrackIndex < arg.tracks.length - 1) {
                        arg.currentTrackIndex++;
                        arg.secondsPlayed = 0;
                        start();
                    }
                    else {
                        arg.secondsPlayed++;
                        setTimeout(() => start(), 1000);
                        throw new Error('No more tracks to play further!');
                    }

                } catch (error) {
                    console.log(error.message);
                }

            } else {
                arg.previousButton = false;
                try {
                    console.log(!arg.secondsPlayed ? `${arg.tracks[arg.currentTrackIndex].trackName} is playing...` : arg.secondsPlayed);

                    if (arg.currentTrackIndex > 0) {
                        arg.currentTrackIndex--;
                        arg.secondsPlayed = 0;
                        start();
                    }
                    else {
                        arg.secondsPlayed++;
                        setTimeout(() => start(), 1000);
                        throw new Error('There is no tracks preceding the current one!');
                    }

                } catch (error) {
                    console.log(error.message);
                }

            }

        } else if (arg.tracks[arg.currentTrackIndex] && arg.secondsPlayed > arg.tracks[arg.currentTrackIndex].trackTime && !arg.pauseButton) {
            arg.secondsPlayed = 0;
            arg.currentTrackIndex++;
            start();
        } else if (arg.tracks[arg.currentTrackIndex] && arg.pauseButton) {
            arg.pauseButton = false;
            console.log(`Pause!`);
        } else {
            arg.secondsPlayed = 0;
            arg.currentTrackIndex = 0;
            console.log(`Done!`);
        }
    }
    start();
})

player.on('next', (arg) => {
    arg.nextButton = true;
});

player.on('previous', (arg) => {
    arg.previousButton = true;
});

player.on('pause', (arg) => {
    arg.pauseButton = true;
});

module.exports = {
    player
}