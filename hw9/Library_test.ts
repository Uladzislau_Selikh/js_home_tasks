import * as sinon from 'sinon';
import { Album } from './Album';
import { Artist } from './Artist';
import { Library } from './Library';
import { Track } from './Track';
import { strict as assert } from 'assert';
import { expect } from 'chai';

let spyOnConsole = sinon.spy(console, `log`);

const Godzilla = new Track('Godzilla', 8, true);

const MusicToBeMurderedBy = new Album('Music To Be Murdered By', new Date('05/11/2020'));
const Eminem = new Artist('Eminem', new Date('12/11/1994'), undefined, [MusicToBeMurderedBy]);

const Darkness = new Track('Darkness', 8, true, MusicToBeMurderedBy, Eminem);
const PlacesToGo = new Track(`PlacesToGo`, 12, true);
const Spring = new Track(`Spring`, 13, false);

const TillLindemann = new Artist('Till Lindemann', new Date('12/11/1976'));

const Rosenrot = new Album('Rosenrot', new Date('11/02/2005'));
const Encore = new Album('Encore', new Date('01/20/2010'));
const Recovery = new Album('Recovery', new Date('04/18/2014'));

const Benzin = new Track('Benzin', 10, false, Rosenrot, TillLindemann);
const LoseYourself = new Track('Lose Yourself', 5, true);
const EightMile = new Album('Eight Mile', new Date('05/11/1995'));

const testLibrary = new Library();

describe('Track & Artist & Album Interaction', () => {
  it(`should automatically add track to artist's track list after assignArtist command or new Track instance declaration`, () => {
    Godzilla.assignArtist(Eminem);
    LoseYourself.assignArtist(Eminem);
    Benzin.assignArtist(TillLindemann);

    expect(Eminem.trackList).to.include.members([Godzilla, LoseYourself, Darkness]);
    expect(TillLindemann.trackList).to.include.members([Benzin]);
  });

  it(`should automatically add track to album's track list after assignAlbum command or new Track instance declaration`, () => {
    Godzilla.assignAlbum(MusicToBeMurderedBy);
    LoseYourself.assignAlbum(EightMile);
    Benzin.assignAlbum(Rosenrot);

    expect(MusicToBeMurderedBy.trackList).to.include.members([Godzilla, Darkness]);
    expect(EightMile.trackList).to.include.members([LoseYourself]);
    expect(Rosenrot.trackList).to.include.members([Benzin]);
  });

  it(`should automatically assign albums to artists after addAlbums command or new Artist instance declaration`, () => {
    Eminem.addAlbums([EightMile]);
    TillLindemann.addAlbums([Rosenrot]);

    assert.strictEqual(EightMile.artist, Eminem);
    assert.strictEqual(MusicToBeMurderedBy.artist, Eminem);
    assert.strictEqual(Rosenrot.artist, TillLindemann);
  });

  it(`should automatically update track's artist assignment after addTracks command`, () => {
    Eminem.addTracks([PlacesToGo]);
    TillLindemann.addTracks([Spring]);

    expect(PlacesToGo.artist).to.be.deep.equal(Eminem);
    expect(Spring.artist).to.be.deep.equal(TillLindemann);
  });

  it(`should automatically update track's album assignment after addTracks command`, () => {
    EightMile.addTracks([PlacesToGo]);
    Rosenrot.addTracks([Spring]);

    expect(PlacesToGo.album).to.be.deep.equal(EightMile);
    expect(Spring.album).to.be.deep.equal(Rosenrot);
  });

  it(`should automatically update album's artist assignment after addAlbums command`, () => {
    Eminem.addAlbums([Encore]);

    expect(Encore.artist).to.be.deep.equal(Eminem);
  });

  it(`should automatically update artist's albumsList after assignArtist command`, () => {
    Recovery.assignArtist(Eminem);

    expect(Eminem.albums).to.include.members([Recovery]);
  });
});

describe('Track Builder', () => {
  it(`should create default Track instance after build() method call with no arguments`, () => {
    expect(Track.build()).to.be.deep.equal(new Track(`Track`, 20, false));
  });

  it(`should create default Track instance after build() method call with only one argument`, () => {
    expect(Track.build(`Sonne`)).to.be.deep.equal(new Track(`Track`, 20, false));
  });

  it(`should create default Track instance after build() method call with only two arguments`, () => {
    expect(Track.build(`Sonne`, 24)).to.be.deep.equal(new Track(`Track`, 20, false));
  });

  it(`should create new Track instance after build() method call with three arguments`, () => {
    expect(Track.build(`Sonne`, 24, false)).to.be.deep.equal(new Track(`Sonne`, 24, false));
  });
});

describe('Album Builder', () => {
  it(`should create default Album instance after build() method call`, () => {
    expect(Album.build()).to.be.deep.equal(new Album(`Album`, new Date(`01/01/2015`)));
  });

  it(`should create default Album instance after build() method call with only one argument`, () => {
    expect(Album.build(`ReiseReise`)).to.be.deep.equal(new Album(`Album`, new Date(`01/01/2015`)));
    expect(Album.build(undefined, new Date(`01/05/2011`))).to.be.deep.equal(new Album(`Album`, new Date(`01/01/2015`)));
  });

  it(`should create new Album instance after build() method call with two arguments`, () => {
    expect(Album.build(`ReiseReise`, new Date(`01/05/2011`))).to.be.deep.equal(
      new Album(`ReiseReise`, new Date(`01/05/2011`))
    );
  });
});

describe('Artist Builder', () => {
  it(`should create default Artist instance after build() method call`, () => {
    expect(Artist.build()).to.be.deep.equal(new Artist(`Artist`, new Date(`01/01/1990`)));
  });

  it(`should create default Artist instance after build() method call with only one argument`, () => {
    expect(Artist.build(`Adele`)).to.be.deep.equal(new Artist(`Artist`, new Date(`01/01/1990`)));
    expect(Artist.build(undefined, new Date(`05/05/1988`))).to.be.deep.equal(
      new Artist(`Artist`, new Date(`01/01/1990`))
    );
  });

  it(`should create new Artist instance after build() method call with two arguments`, () => {
    expect(Artist.build(`Adele`, new Date(`05/05/1988`))).to.be.deep.equal(new Artist(`Adele`, new Date(`05/05/1988`)));
  });
});

describe('Library add method', () => {
  it(`should add items to the Library as separate arguments`, () => {
    testLibrary.add(EightMile, LoseYourself);
    expect(testLibrary.albums).to.include.members([EightMile]);
    expect(testLibrary.tracks).to.include.members([LoseYourself]);
  });

  it(`should add items to the Library as array structure`, () => {
    testLibrary.add(Darkness, [TillLindemann, Benzin], [Eminem]);
    expect(testLibrary.artists).to.include.members([TillLindemann, Eminem]);
    expect(testLibrary.tracks).to.include.members([Darkness, Benzin]);
  });

  it(`should throw an error if the item is already in the Library`, () => {
    testLibrary.add([LoseYourself, MusicToBeMurderedBy], Benzin);
    assert(spyOnConsole.calledWithExactly(`The element 1 of the array 1 is already in the Library!`));
    assert(spyOnConsole.calledWithExactly(`The element 2 is already in the Library!`));
  });

  it(`should throw an error if the item is not a Track & Album & Artist instance`, () => {
    testLibrary.add([1, `Track`, Encore], true, Godzilla);
    assert(spyOnConsole.calledWithExactly(`Please enter valid and existing object name for element 1 of the array 1!`));
    assert(spyOnConsole.calledWithExactly(`Please enter valid and existing object name for element 2 of the array 1!`));
    assert(spyOnConsole.calledWithExactly(`Please enter valid and existing object name!`));
  });
});

describe('Library delete method', () => {
  it(`should delete items from the Library as separate arguments`, () => {
    testLibrary.delete(TillLindemann, Encore);
    expect(testLibrary.albums).not.to.include.members([Encore]);
    expect(testLibrary.artists).not.to.include.members([TillLindemann]);
  });

  it(`should delete items from the Library as array structure`, () => {
    testLibrary.delete([EightMile, LoseYourself], Godzilla);
    expect(testLibrary.albums).not.to.include.members([EightMile]);
    expect(testLibrary.tracks).not.to.include.members([LoseYourself, Godzilla]);
  });

  it(`should throw an error if the item is not in the Library or of incompatible format`, () => {
    testLibrary.delete(Godzilla, [EightMile, LoseYourself], `Album`);
    assert(
      spyOnConsole.calledWithExactly(`The Library doesn't contain item 1 you have passed! There is nothing to delete!`)
    );
    assert(
      spyOnConsole.calledWithExactly(`The Library doesn't contain item 3 you have passed! There is nothing to delete!`)
    );
    assert(
      spyOnConsole.calledWithExactly(`The Library doesn't contain item 1 of the array 2! There is nothing to delete!`)
    );
    assert(
      spyOnConsole.calledWithExactly(`The Library doesn't contain item 2 of the array 2! There is nothing to delete!`)
    );
  });
});

describe('Library update method', () => {
  before(() => {
    testLibrary.add([Godzilla], TillLindemann, Rosenrot);
  });

  it(`should update Library items properties`, () => {
    testLibrary.update(Godzilla, 'trackTime', 15);
    testLibrary.update(Benzin, 'explicit', true);
    testLibrary.update(MusicToBeMurderedBy, 'albumName', `MTBMB`);
    testLibrary.update(TillLindemann, 'artistBirth', new Date('04/18/1986'));

    expect(Godzilla.trackTime).to.be.deep.equal(15);
    expect(Benzin.explicit).to.be.deep.equal(true);
    expect(MusicToBeMurderedBy.albumName).to.be.deep.equal(`MTBMB`);
    expect(TillLindemann.artistBirth).to.be.deep.equal(new Date('04/18/1986'));
  });

  it(`should throw an error if the property doesn't exist in the object`, () => {
    testLibrary.update(Godzilla, 'property', 15);
    testLibrary.update(MusicToBeMurderedBy, 'explicit', `MTBMB`);
    testLibrary.update(TillLindemann, 'albumName', new Date('04/18/1986'));

    assert(
      spyOnConsole.calledWithExactly(`Please check the property name and new value type! There is nothing to update!`)
    );
    expect(Object.keys(Godzilla)).not.to.include.members(['property']);
    expect(Object.keys(MusicToBeMurderedBy)).not.to.include.members(['explicit']);
    expect(Object.keys(TillLindemann)).not.to.include.members(['albumName']);
  });

  it(`should throw an error if the item is not in the Library`, () => {
    testLibrary.update(LoseYourself, 'trackTime', 20);
    testLibrary.update(EightMile, 'releaseDate', new Date('02/15/2009'));
    testLibrary.update(PlacesToGo, 'explicit', false);

    assert(
      spyOnConsole.calledWithExactly(`The Library doesn't contain item you have passed! There is nothing to update!`)
    );
    expect(LoseYourself.trackTime).to.be.deep.equal(5);
    expect(EightMile.releaseDate).to.be.deep.equal(new Date('05/11/1995'));
    expect(PlacesToGo.explicit).to.be.deep.equal(true);
  });
});

describe('Library display method', () => {
  it(`should display actual Library items`, () => {
    expect(testLibrary.display()).to.be.deep.equal({
      tracks: ['Darkness', 'Benzin', 'Godzilla'],
      albums: ['MTBMB', 'Rosenrot'],
      artists: ['Eminem', 'Till Lindemann']
    });

    testLibrary.add(Encore, Recovery);
    testLibrary.delete([Darkness, TillLindemann], MusicToBeMurderedBy);

    expect(testLibrary.display()).to.be.deep.equal({
      tracks: ['Benzin', 'Godzilla'],
      albums: ['Rosenrot', `Encore`, `Recovery`],
      artists: ['Eminem']
    });

    testLibrary.delete([Benzin, Godzilla], [Rosenrot, Encore, Recovery]);

    expect(testLibrary.display()).to.be.deep.equal({
      tracks: [],
      albums: [],
      artists: ['Eminem']
    });
  });
});

describe('Library search method', () => {
  before(() => {
    testLibrary.add([Godzilla], TillLindemann, LoseYourself, Rosenrot, [Benzin, Encore, Recovery]);
  });

  it(`should display search results`, () => {
    testLibrary.search('se');
    testLibrary.search('Godzilla');
    testLibrary.search('');
    testLibrary.search('Godjirraa');

    setTimeout(
      () =>
        assert(
          spyOnConsole.calledWithExactly({
            tracks: [`Lose Yourself`],
            albums: [`Rosenrot`]
          })
        ),
      2000
    );

    setTimeout(
      () =>
        assert(
          spyOnConsole.calledWithExactly({
            tracks: [`Godzilla`]
          })
        ),
      2000
    );

    setTimeout(
      () =>
        assert(
          spyOnConsole.calledWithExactly({
            tracks: [`Godzilla`, `Lose Yourself`, `Benzin`],
            albums: [`Rosenrot`, `Encore`, `Recovery`],
            artists: ['Eminem', `Till Lindemann`]
          })
        ),
      2000
    );

    setTimeout(() => assert(spyOnConsole.calledWithExactly(`No results found!`)), 2000);
  });
});
