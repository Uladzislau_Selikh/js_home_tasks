const {by, element, $} = require("protractor");
const BasePage = require("./base_page");
const DriverUtils = require("../utils/driver_utils");

class LoginPage extends BasePage {

    constructor() {
        super();
    };

    get loginInput() {
        return element(by.name("login"));
    };

    get passwordInput() {
        return element(by.name("password"));
    };

    get submitButton() {
        return element(by.css("input[type=submit]"));
    };

    getErrorMessage() {
        return $(".flash.flash-full.flash-error").getText();
    }

    clickForgotPasswordLink = async () => {
        await DriverUtils.clickElement(element(by.xpath(`//a[text()[contains(.,"Forgot password?")]]`)));
    }

    performLogin = async (login, password) => {
        await DriverUtils.enterText(this.loginInput, login);
        await DriverUtils.enterText(this.passwordInput, password);
        await DriverUtils.clickElement(this.submitButton);
    };

};

module.exports = new LoginPage();