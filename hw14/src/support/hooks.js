const { browser } = require("protractor");
const { defaultTimeoutMs } = require("./constants");
const { After, setDefaultTimeout } = require(`@cucumber/cucumber`);

setDefaultTimeout(240 * 1000);

// eslint-disable-next-line new-cap
After(async function() {
    let imageData = await browser.takeScreenshot();
    // eslint-disable-next-line no-invalid-this
    await this.attach(Buffer.from(imageData, 'base64'), 'image/png');
})
