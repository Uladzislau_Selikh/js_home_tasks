import { Album } from './Album';
import { Track } from './Track';
export class Artist {
  artistName: string;

  artistBirth: Date;

  albums: Album[];

  trackList: Track[];

  [prop: string]: unknown;

  constructor(name: string, birth: Date, tracks?: Track[], albums?: Album[]) {
    this.artistName = name;
    this.artistBirth = birth;
    this.albums = albums || [];
    this.trackList = tracks || [];
    tracks?.forEach((item) => {
      item.artist = this;
    });
    albums?.forEach((item) => {
      item.artist = this;
    });
  }

  static build(name?: string, birth?: Date, tracks?: Track[], albums?: Album[]): Artist {
    return name && birth ? new Artist(name, birth, tracks, albums) : new Artist(`Artist`, new Date(`01/01/1990`));
  }

  public addTracks(tracks: Track[]): void {
    tracks.forEach((element) => {
      this.trackList.push(element);
      element.artist = this;
    });
  }

  public addAlbums(albums: Album[]): void {
    albums.forEach((element) => {
      this.albums.push(element);
      element.artist = this;
    });
  }
}
