const { resolve } = require(`path`);
const shellExecutor = require(`shelljs`);
const reportsDir = `./hw16/reports`;
const logger = require(`./configLogger`).logger;

exports.config = {
    directConnect: true,
    capabilities: {
        acceptInsecureCerts: true,
        browserName: 'chrome',
        screenResolution: '1920x1080',
        chromeOptions: {
            args: ["--incognito", "--no-sandbox", "--disable-web-security", "--allow-running-insecure-content", "--disable-gpu", "--headless", "--start-maximized", "--window-size=1920x1080", "disable-extensions", "--disable-infobars"],
            prefs: {
                // eslint-disable-next-line camelcase
                credentials_enable_service: false,
                profile: {
                    // eslint-disable-next-line camelcase
                    password_manager_enabled: false
                }
            }
        },
    },
    cucumberOpts: {
        require: [resolve(`./hw16/src/step_definitions/**/*.js`), resolve(`./hw16/src/support/**/*.js`)],
        format: [`@cucumber/pretty-formatter`, `json:${reportsDir}/report.json`],
        tags: [`@navigation`, `@login`, `@textVerify`, `@redirection`, `@forgotPassword`, `@invalidUsername`, `@invalidPassword`, `@validCredentials`]
    },

    allScriptsTimeout: 360000,
    getPageTimeout: 360000,

    baseUrl: 'https://github.com/',

    framework: `custom`,
    frameworkPath: require.resolve(`protractor-cucumber-framework`),
    specs: [resolve(`./hw16/features/**/*.feature`)],

    onPrepare: async () => {
        // eslint-disable-next-line no-undef
        await browser.waitForAngularEnabled(false);
        logger.info(`Deleting previous HTML report (if any) and clean log file content...`)
        shellExecutor.cd(`${reportsDir}`);
        shellExecutor.exec(`del *.html`);
        logger.warn(`Launching browser in headless mode...`)
    },

    onCleanUp: () => {
        logger.verbose(`The full log of test execution can be found by following the path ./hw16/logs`)
        shellExecutor.exec(`taskkill /IM chromedriver* /F /T`, { silent: true });
    },

    afterLaunch: () => {
        shellExecutor.exec(`npm run hw16:test:report`, { silent: true });
    }
}
