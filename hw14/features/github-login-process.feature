@login
Feature: Github login process

    Background:
        Given open web page https://github.com/login

    @forgotPassword
    Scenario: Forgot password
        When click on Forgot password link
        Then the user is on the password reset page

    @invalidUsername
    Scenario: Invalid username
        When log in with invalid username of 10 symbols length
        Then an error message of invalid credentials appears

    @invalidPassword
    Scenario: Invalid password
        When log in with invalid password of 9 symbols length
        Then an error message of invalid credentials appears
    
    @validCredentials
    Scenario: Valid credentials
        When log in with valid credentials
        Then the user is redirected from sign-in page