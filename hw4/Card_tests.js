const credit = require('./Card').credit;
const debit = require('./Card').debit;
let assert = require('assert');

describe('Card task unit testing', function () {

    it('As a user I want to see my card balance.', function () {
        assert.strictEqual(credit.getBalance, `Dear Uladzislau,\nYour current balance is 1000 BYN!`);
        assert.strictEqual(debit.getBalance, `Dear Vitali,\nYour current balance is 1200 BYN!`);
    });

      it('As a user I want to reduce my card balance and see the resulting balance.', function () {
        assert.strictEqual(credit.reduceBalance(300), `Dear Uladzislau,\nYour balance is reduced by 300 BYN.\nThe current balance is 700 BYN.`);
        assert.strictEqual(credit.reduceBalance(200), `Dear Uladzislau,\nYour balance is reduced by 200 BYN.\nThe current balance is 500 BYN.`);
        assert.strictEqual(credit.reduceBalance(600), `Dear Uladzislau,\nYour balance is reduced by 600 BYN.\nThe current balance is -100 BYN.`);

        assert.strictEqual(debit.reduceBalance(300), `Dear Vitali,\nYour balance is reduced by 300 BYN.\nThe current balance is 900 BYN.`);
        assert.strictEqual(debit.reduceBalance(1000), `Not enough cash! Please enter another amount!`);
        assert.strictEqual(debit.reduceBalance(100), `Dear Vitali,\nYour balance is reduced by 100 BYN.\nThe current balance is 800 BYN.`);
    });

    it('As a user I want to add money to the card and see the balance.', function () {
        assert.strictEqual(credit.addToBalance(300), `Dear Uladzislau,\nYou have added 300 BYN to your card.\nThe current balance is 200 BYN.`);
        assert.strictEqual(credit.addToBalance(200), `Dear Uladzislau,\nYou have added 200 BYN to your card.\nThe current balance is 400 BYN.`);
        assert.strictEqual(credit.addToBalance(500), `Dear Uladzislau,\nYou have added 500 BYN to your card.\nThe current balance is 900 BYN.`);

        assert.strictEqual(debit.addToBalance(100), `Dear Vitali,\nYou have added 100 BYN to your card.\nThe current balance is 900 BYN.`);
        assert.strictEqual(debit.addToBalance(200), `Dear Vitali,\nYou have added 200 BYN to your card.\nThe current balance is 1100 BYN.`);
        assert.strictEqual(debit.addToBalance(500), `Dear Vitali,\nYou have added 500 BYN to your card.\nThe current balance is 1600 BYN.`);
    });

    it('As a user I want to view my current balance in USD currency.', function () {
        assert.strictEqual(credit.getBalanceInCurrency(1.523), `Dear Uladzislau,\nYour balance in currency calculated with 1.523 exchange rate is:\n591 USD.`);
        assert.strictEqual(credit.getBalanceInCurrency(1.89), `Dear Uladzislau,\nYour balance in currency calculated with 1.89 exchange rate is:\n476 USD.`);
        assert.strictEqual(credit.getBalanceInCurrency(2.345), `Dear Uladzislau,\nYour balance in currency calculated with 2.345 exchange rate is:\n384 USD.`);
    
        assert.strictEqual(debit.getBalanceInCurrency(1.523), `Dear Vitali,\nYour balance in currency calculated with 1.523 exchange rate is:\n1051 USD.`);
        assert.strictEqual(debit.getBalanceInCurrency(1.89), `Dear Vitali,\nYour balance in currency calculated with 1.89 exchange rate is:\n847 USD.`);
        assert.strictEqual(debit.getBalanceInCurrency(2.345), `Dear Vitali,\nYour balance in currency calculated with 2.345 exchange rate is:\n682 USD.`);
    });

    it('As a user I want to protect my card from manual balance change.', function () {
        let newBalance = 2500;
        credit.balance = newBalance;
        assert.strictEqual(credit.getBalance, `Dear Uladzislau,\nYour current balance is 900 BYN!`);
        debit.balance = newBalance;
        assert.strictEqual(debit.getBalance, `Dear Vitali,\nYour current balance is 1600 BYN!`);
    });

});
