const player = require('./Player').player;
class Library {
    constructor() {
        this.tracks = [];
        this.albums = [];
        this.artists = [];

        this.currentTrackIndex = 0;
        this.secondsPlayed = 0;

        this.nextButton = false;
        this.previousButton = false;
        this.pauseButton = false;
    }

    play() {
        player.emit('play', this);
    }

    next() {
        player.emit('next', this);
    }

    previous() {
        player.emit('previous', this);
    }

    pause() {
        player.emit('pause', this);
    }

    add(...items) {
        for (let i = 0; i < items.length; i++) {
            try {
                if (Array.isArray(items[i])) {
                    items[i].forEach((element, index) => {
                        try {
                            if (element.trackName) {
                                if (this.tracks.indexOf(element) === -1) this.tracks.push(element)
                                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
                            } else if (element.albumName) {
                                if (this.albums.indexOf(element) === -1) this.albums.push(element)
                                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
                            } else if (element.artistName) {
                                if (this.artists.indexOf(element) === -1) this.artists.push(element)
                                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
                            } else throw new Error(`Please enter valid and existing object name for element ${index + 1} of the array ${i + 1}!`);
                        } catch (error) {
                            console.log(error.message);
                        }
                    });
                } else if (items[i].trackName) {
                    if (this.tracks.indexOf(items[i]) === -1) this.tracks.push(items[i])
                    else throw new Error(`The element ${i + 1} is already in the Library!`);
                } else if (items[i].albumName) {
                    if (this.albums.indexOf(items[i]) === -1) this.albums.push(items[i])
                    else throw new Error(`The element ${i + 1} is already in the Library!`);
                } else if (items[i].artistName) {
                    if (this.artists.indexOf(items[i]) === -1) this.artists.push(items[i])
                    else throw new Error(`The element ${i + 1} is already in the Library!`);
                } else throw new Error('Please enter valid and existing object name!');
            } catch (error) {
                console.log(error.message);
            }
        }
    }

    delete(...items) {
        for (let i = 0; i < items.length; i++) {
            let itemIndex;
            try {
                if (items[i].trackName && this.tracks.indexOf(items[i]) !== -1) {
                    itemIndex = this.tracks.indexOf(items[i])
                    this.tracks.splice(itemIndex, 1);
                } else if (items[i].albumName && this.albums.indexOf(items[i]) !== -1) {
                    itemIndex = this.albums.indexOf(items[i])
                    this.albums.splice(itemIndex, 1);
                } else if (items[i].artistName && this.artists.indexOf(items[i]) !== -1) {
                    itemIndex = this.artists.indexOf(items[i])
                    this.artists.splice(itemIndex, 1);
                } else throw new Error(`The Library doesn't contain item ${i + 1} you have passed! There is nothing to delete!`);
            } catch (error) {
                console.log(error.message);
            }
        }
    }

    update(item, prop, newValue) {
        try {
            if (item[prop]) {

                if (this.tracks.indexOf(item) !== -1 || this.albums.indexOf(item) !== -1 || this.artists.indexOf(item) !== -1) {
                    item[prop] = newValue;
                } else throw new Error(`The Library doesn't contain item you have passed! There is nothing to update!`);

            }
            else throw new Error(`Check the name of the property you have passed! There is nothing to update!`);
        } catch (error) {
            console.log(error.message);
        }
    }

    display() {
        let result = {
            tracks: [],
            albums: [],
            artists: [],
        }

        for (let prop in this) {
            if (result[prop]) {
            this[prop].map(item => {
                result[prop].push(item.artistName ? item.artistName : item.trackName ? item.trackName : item.albumName);
            });
        }
        }

        console.log(result);
    }

    search(itemName) {
        return new Promise((resolve, reject) => {
            let searchResult = {};
            let categoryFilter;

            for (let prop in this) {
                if (prop === `tracks`) {
                    categoryFilter = this[prop].map(item => item.trackName).filter(item => item.includes(itemName))
                    if (categoryFilter.length) searchResult[prop] = categoryFilter;
                } else if (prop === `albums`) {
                    categoryFilter = this[prop].map(item => item.albumName).filter(item => item.includes(itemName))
                    if (categoryFilter.length) searchResult[prop] = categoryFilter;
                } else if (prop === `artists`) {
                    categoryFilter = this[prop].map(item => item.artistName).filter(item => item.includes(itemName))
                    if (categoryFilter.length) searchResult[prop] = categoryFilter;
                }
            }

            setTimeout(() => {
                if (Object.keys(searchResult).length > 0) {
                resolve(console.log(searchResult));
            } else {
                reject(new Error(`No results found!`).message);
            }}, 300);
            
        }).catch(console.log);
    }
}

module.exports = {
    Library
}