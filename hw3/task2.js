function Card(balance = 0) {
    this.name = process.env.USERNAME;
    this.balance = balance;

    Object.defineProperty(this, 'balance', {
        writable: false,
    });

    this.getBalance = function getBalance() {
        return `Dear ${process.env.USERNAME},\nYour current balance is ${this.balance} BYN.`;
    }

    this.addToBalance = function addToBalance(addition) {
        Object.defineProperty(this, 'balance', {
            writable: true,
        });

        this.balance += addition;

        Object.defineProperty(this, 'balance', {
            writable: false,
        });

        return `Dear ${process.env.USERNAME},\nYou have added ${addition} BYN to your card.\nThe current balance is ${this.balance} BYN.`;
    }

    this.reduceBalance = function reduceBalance(reducer) {
        Object.defineProperty(this, 'balance', {
            writable: true,
        });

        this.balance -= reducer;

        Object.defineProperty(this, 'balance', {
            writable: false,
        });

        return `Dear ${process.env.USERNAME},\nYour balance is reduced by ${reducer} BYN.\nThe current balance is ${this.balance} BYN.`;
    }

    this.getBalanceInCurrency = function getBalanceInCurrency(exchRate) {
        return `Dear ${process.env.USERNAME},\nYour balance in currency calculated with ${exchRate} exchange rate is:\n${Math.round(this.balance / exchRate)} USD.`;
    }
}

let card = new Card(1500);

module.exports = {
    card
}