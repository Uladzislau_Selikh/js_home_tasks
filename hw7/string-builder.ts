class Builder {
    build() {
        throw new Error('Never use super.build()');
    }
}

export class StringBuilder extends Builder {
    content: string;

    constructor(string = '') {
        super();
        this.content = string;
    }

    append(string: string) {
        this.content += string;
        return this;
    }

    delete(start: number, count: number) {
        this.content = this.content.substring(0, start) + this.content.substring(start + count, this.content.length);
        return this;
    }

    build() {
        return this.content;
    }
}