import { Album } from './Album';
import { Artist } from './Artist';
import { Track } from './Track';
import { player } from './Player';

const libraryPlayer = player;

function isTrack(item: unknown): item is Track {
  return (item as Track).trackName !== undefined;
}

function isAlbum(item: unknown): item is Album {
  return (item as Album).albumName !== undefined;
}

function isArtist(item: unknown): item is Artist {
  return (item as Artist).artistName !== undefined;
}
export class Library {
  public tracks!: Track[];

  public albums!: Album[];

  public artists!: Artist[];

  [prop: string]: unknown;

  public currentTrackIndex!: number;

  public secondsPlayed!: number;

  public nextButton!: boolean;

  public previousButton!: boolean;

  public pauseButton!: boolean;

  constructor() {
    this.tracks = [];
    this.albums = [];
    this.artists = [];

    this.currentTrackIndex = 0;
    this.secondsPlayed = 0;

    this.nextButton = false;
    this.previousButton = false;
    this.pauseButton = false;
  }

  public play(): void {
    libraryPlayer.emit('play', this);
  }

  public next(): void {
    libraryPlayer.emit('next', this);
  }

  public previous(): void {
    libraryPlayer.emit('previous', this);
  }

  public pause(): void {
    libraryPlayer.emit('pause', this);
  }

  public add(...items: unknown[]): void {
    for (let i = 0; i < items.length; i++) {
      try {
        if (Array.isArray(items[i])) {
          (items[i] as (Track | Album | Artist)[]).forEach((element, index) => {
            try {
              if (isTrack(element)) {
                if (this.tracks.indexOf(element) === -1) this.tracks.push(element);
                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
              } else if (isAlbum(element)) {
                if (this.albums.indexOf(element) === -1) this.albums.push(element);
                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
              } else if (isArtist(element)) {
                if (this.artists.indexOf(element) === -1) this.artists.push(element);
                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
              } else
                throw new Error(
                  `Please enter valid and existing object name for element ${index + 1} of the array ${i + 1}!`
                );
            } catch (error) {
              console.log(error.message);
            }
          });
        } else if (isTrack(items[i])) {
          if (this.tracks.indexOf(items[i] as Track) === -1) this.tracks.push(items[i] as Track);
          else throw new Error(`The element ${i + 1} is already in the Library!`);
        } else if (isAlbum(items[i])) {
          if (this.albums.indexOf(items[i] as Album) === -1) this.albums.push(items[i] as Album);
          else throw new Error(`The element ${i + 1} is already in the Library!`);
        } else if (isArtist(items[i])) {
          if (this.artists.indexOf(items[i] as Artist) === -1) this.artists.push(items[i] as Artist);
          else throw new Error(`The element ${i + 1} is already in the Library!`);
        } else throw new Error('Please enter valid and existing object name!');
      } catch (error) {
        console.log(error.message);
      }
    }
  }

  public delete(...items: unknown[]): void {
    for (let i = 0; i < items.length; i++) {
      let itemIndex: number;
      try {
        if (Array.isArray(items[i])) {
          (items[i] as (Track | Album | Artist)[]).forEach((element, index) => {
            try {
              if (isTrack(element) && this.tracks.indexOf(element) !== -1) {
                itemIndex = this.tracks.indexOf(element);
                this.tracks.splice(itemIndex, 1);
              } else if (isAlbum(element) && this.albums.indexOf(element) !== -1) {
                itemIndex = this.albums.indexOf(element);
                this.albums.splice(itemIndex, 1);
              } else if (isArtist(element) && this.artists.indexOf(element) !== -1) {
                itemIndex = this.artists.indexOf(element);
                this.artists.splice(itemIndex, 1);
              } else
                throw new Error(
                  `The Library doesn't contain item ${index + 1} of the array ${i + 1}! There is nothing to delete!`
                );
            } catch (error) {
              console.log(error.message);
            }
          });
        } else if (isTrack(items[i]) && this.tracks.indexOf(items[i] as Track) !== -1) {
          itemIndex = this.tracks.indexOf(items[i] as Track);
          this.tracks.splice(itemIndex, 1);
        } else if (isAlbum(items[i]) && this.albums.indexOf(items[i] as Album) !== -1) {
          itemIndex = this.albums.indexOf(items[i] as Album);
          this.albums.splice(itemIndex, 1);
        } else if (isArtist(items[i]) && this.artists.indexOf(items[i] as Artist) !== -1) {
          itemIndex = this.artists.indexOf(items[i] as Artist);
          this.artists.splice(itemIndex, 1);
        } else
          throw new Error(`The Library doesn't contain item ${i + 1} you have passed! There is nothing to delete!`);
      } catch (error) {
        console.log(error.message);
      }
    }
  }

  public update<T extends [Track | Artist | Album, string, unknown]>(...args: T): void {
    try {
      if (args[0][args[1]] !== undefined) {
        if (
          this.tracks.indexOf(args[0] as Track) !== -1 ||
          this.albums.indexOf(args[0] as Album) !== -1 ||
          this.artists.indexOf(args[0] as Artist) !== -1
        ) {
          args[0][args[1]] = args[2];
        } else throw new Error(`The Library doesn't contain item you have passed! There is nothing to update!`);
      } else throw new Error(`Please check the property name and new value type! There is nothing to update!`);
    } catch (error) {
      console.log(error.message);
    }
  }

  public display(): unknown {
    interface Result {
      tracks: string[];
      albums: string[];
      artists: string[];
      [prop: string]: string[];
    }

    let result: Result = {
      tracks: [],
      albums: [],
      artists: []
    };

    for (let prop in result) {
      if (this[prop]) {
        (this[prop] as (Artist | Album | Track)[]).map((item) => {
          result[prop].push(
            (item as Artist).artistName
              ? (item as Artist).artistName
              : (item as Track).trackName
              ? (item as Track).trackName
              : (item as Album).albumName
          );
        });
      }
    }

    return result;
  }

  public search(itemName: string): unknown {
    return new Promise((resolve, reject) => {
      interface searchResult {
        [prop: string]: string[];
      }

      let searchResult: searchResult = {};
      let categoryFilter;

      for (let prop in this) {
        if (prop === `tracks`) {
          categoryFilter = (this[prop] as Track[])
            .map((item) => item.trackName)
            .filter((item) => item.includes(itemName));
          if (categoryFilter.length) searchResult[prop] = categoryFilter;
        } else if (prop === `albums`) {
          categoryFilter = (this[prop] as Album[])
            .map((item) => item.albumName)
            .filter((item) => item.includes(itemName));
          if (categoryFilter.length) searchResult[prop] = categoryFilter;
        } else if (prop === `artists`) {
          categoryFilter = (this[prop] as Artist[])
            .map((item) => item.artistName)
            .filter((item) => item.includes(itemName));
          if (categoryFilter.length) searchResult[prop] = categoryFilter;
        }
      }

      setTimeout(() => {
        if (Object.keys(searchResult).length > 0) {
          resolve(console.log(searchResult));
        } else {
          reject(new Error(`No results found!`).message);
        }
      }, 300);
    }).catch(console.log);
  }
}

type OneArgumentType = `search`;
type ThreeArgumentsType = `update`;
type ManyArgumentsType = `add` | `delete`;

function isOneArgumentType(item: keyof Library): item is OneArgumentType {
  return item === `search`;
}

function isThreeArgumentsType(item: keyof Library): item is ThreeArgumentsType {
  return item === `update`;
}

function isManyArgumentsType(item: keyof Library): item is ManyArgumentsType {
  return item === `add` || item === `delete`;
}
interface Command {
  execute(action: string, ...args: unknown[]): unknown;
}
export class LibraryCommand implements Command {
  constructor(private readonly library: Library) {}

  public execute<A extends [Artist | Track | Album, string, unknown] | unknown[]>(
    action: keyof Library,
    ...items: A
  ): unknown {
    try {
      if (isOneArgumentType(action)) this.library[action](items[0] as string);
      else if (isThreeArgumentsType(action))
        return this.library[action](...(items as [Artist | Track | Album, string, unknown]));
      else if (isManyArgumentsType(action)) return this.library[action](...items);
      else return (this.library[action] as () => unknown)();
    } catch (error) {
      console.log(`Invalid command or parameter passed!`);
    }
  }
}
