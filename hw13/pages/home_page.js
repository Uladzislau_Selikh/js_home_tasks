const {by, element} = require("protractor");
const BasePage = require("./base_page");
const DriverUtils = require("../utils/driver_utils");


class WelcomePage extends BasePage {

    constructor() {
        super();
    };

    scrollToNavigationLinks = async () => {
        await DriverUtils.scrollToElement(element(by.css(`nav.home-nav-links`)));
    };

    scrollToHeader = async () => {
        await DriverUtils.scrollToElement(element(by.xpath(`//a[@href='/login']`)));
    };

    getNavigationLinks = () => {
        return element.all(by.className(`home-nav-item`));
    };

    getSignInButton = () => {
        return element(by.xpath(`//a[@href='/login']`));
    };
};

module.exports = new WelcomePage();