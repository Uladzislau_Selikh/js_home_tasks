function fibonacci(limit) {
    let prev = 0;
    console.log(prev);
    let curr = 1;
    let temp = prev;
    let count = 1;
    while (curr <= limit) {
        console.log(curr);
        temp = curr;
        curr += prev;
        prev = temp;
        count += 1;
    }
    console.log(`The number of Fibonacci numbers from 0 to ${limit} is ${count}.` + '\n');
}
module.exports = { fibonacci }