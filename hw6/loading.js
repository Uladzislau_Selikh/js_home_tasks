const util = require('util');

const MAX_VALUE = 10
const spinnerElements = [`|`, `/`, `-`, `\\`];
const delay = util.promisify((time, callback) => setTimeout(() => callback(), time));

module.exports = function loading(times = 0) {
  if (times >= MAX_VALUE) {
    return new Promise((resolve) => {
      delay(500, resolve);
    }).then(() => {
      process.stdout.write(`\r`);
    });
  } else {
    return new Promise((resolve) => {
      delay(500, resolve);
    }).then(() => {
      process.stdout.write(`\r${spinnerElements[times % 4]}`);
      return loading(++times);
    });
  }
}