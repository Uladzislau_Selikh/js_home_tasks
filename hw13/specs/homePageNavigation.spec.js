const { expect } = require("chai");
const { baseUrl } = require("../utils/constants");
const { browser } = require("protractor");
const DriverUtils = require("../utils/driver_utils");
const WelcomePage = require("../pages/home_page");

describe("Git Hub Main Page Navigation", () => {

    it("Should display correct text of the navigation panel links", async () => {
        await browser.get(baseUrl);
        await WelcomePage.scrollToNavigationLinks();

        const navigationItems = ['Code', 'Collaborate', 'Develop', 'Automate', 'Secure', 'Community'];
        await WelcomePage.getNavigationLinks().each(async (item, index) => {
            await DriverUtils.getElementInnerText(item)
            .then(text => {
                expect(text).to.be.deep.equal(navigationItems[index]);
            })
        })
    });

    it("Should redirect user to sign-in page", async () => {
        await WelcomePage.scrollToHeader();
        await WelcomePage.getSignInButton().click();
        await WelcomePage.getCurrentUrl
        .then((item) => {
            expect(item).to.contain(`https://github.com/login`);
        });
        await browser.sleep(2000);
    });

});