const { browser, by, element, $ } = require("protractor");
const BasePage = require("./base_page");
const { DriverUtils, highlightElement } = require("../support/driver_utils");
const logger = require("../../config/configLogger").logger;

class LoginPage extends BasePage {
    constructor() {
        super();
    }

    get loginInput() {
        return element(by.name("login"));
    }

    get passwordInput() {
        return element(by.name("password"));
    }

    get submitButton() {
        return element(by.css("input[type=submit]"));
    }

    getErrorMessage = async () => {
        const message = await $(".flash.flash-full.flash-error");

        return DriverUtils.getElementInnerText(message);
    }

    clickForgotPasswordLink = async () => {
        const link = await element(by.xpath(`//a[text()[contains(.,"Forgot password?")]]`)).getWebElement();
        await DriverUtils.clickElement(link);
        logger.info(`Clicking on the link...`);
        logger.warn(`Redirecting to the password reset page...`);
    }

    performLogin = async (login, password) => {
        // eslint-disable-next-line no-invalid-this
        await DriverUtils.enterText(this.loginInput, login);
        logger.info(`Passing text to the login input...`);
        // eslint-disable-next-line no-invalid-this
        await DriverUtils.enterText(this.passwordInput, password);
        logger.info(`Passing text to the password input...`);
        // eslint-disable-next-line no-invalid-this
        await DriverUtils.clickElement(this.submitButton);
        logger.info(`Clicking on the Submit button...`);
    }
}

module.exports = new LoginPage()
