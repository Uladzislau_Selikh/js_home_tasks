import { Album } from "./Album";
import { Artist } from "./Artist";
export class Track {

  trackName: string;
  trackTime: number;
  album: Album | null;
  artist: Artist | null;
  explicit: boolean;
  [prop: string]: unknown;

  constructor(name: string, time: number, explicit: boolean, album?: Album, artist?: Artist) {
    this.trackName = name;
    this.trackTime = time;
    this.album = album || null;
    this.artist = artist || null;
    this.explicit = explicit;
    if (artist?.trackList.indexOf(this) === -1) artist?.trackList.push(this);
    if (album?.trackList.indexOf(this) === -1) album?.trackList.push(this);
  }

  static build(name: string, time: number, explicit: boolean, album?: Album, artist?: Artist) {
    return name && time && explicit ? new Track(name, time, explicit, album, artist) : new Track(`Track`, 20, false);
  }

  public assignArtist(artist: Artist) {
    this.artist = artist;
    if (artist.trackList.indexOf(this) === -1) artist.trackList.push(this);
  }

  public assignAlbum(album: Album) {
    this.album = album;
    if (album.trackList.indexOf(this) === -1) album.trackList.push(this);
  }
}