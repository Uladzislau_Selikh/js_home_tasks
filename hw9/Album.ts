import { Artist } from './Artist';
import { Track } from './Track';
export class Album {
  albumName: string;

  releaseDate: Date;

  trackList: Track[];

  artist: Artist | null;

  [prop: string]: unknown;

  constructor(name: string, date: Date, tracks?: Track[], artist?: Artist) {
    this.albumName = name;
    this.releaseDate = date;
    this.trackList = tracks || [];
    this.artist = artist || null;
    tracks?.forEach((item) => {
      item.album = this;
    });
    if (artist?.albums.indexOf(this) === -1) artist?.albums.push(this);
  }

  static build(name?: string, date?: Date, tracks?: Track[], artist?: Artist): Album {
    return name && date ? new Album(name, date, tracks, artist) : new Album(`Album`, new Date(`01/01/2015`));
  }

  public assignArtist(artist: Artist): void {
    this.artist = artist;
    if (artist.albums.indexOf(this) === -1) artist.albums.push(this);
  }

  public addTracks(tracks: Track[]): void {
    tracks.forEach((element) => {
      this.trackList.push(element);
      element.album = this;
    });
  }
}
