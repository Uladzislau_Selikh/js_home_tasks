import { Album } from './Album';
import { Artist } from './Artist';

function isSeconds(time: unknown): time is number {
  return typeof time === `number`;
}
export class Track {
  trackName: string;

  trackTime!: number;

  album: Album | null;

  artist: Artist | null;

  explicit: boolean;

  [prop: string]: unknown;

  constructor(name: string, time: unknown, explicit: boolean, album?: Album, artist?: Artist) {
    this.trackName = name;
    try {
      if (isSeconds(time)) this.trackTime = time;
      else throw new Error(`Duration parameter has an improper type!`);
    } catch (error) {
      console.log(error.message);
    }
    this.album = album || null;
    this.artist = artist || null;
    this.explicit = explicit;
    if (artist?.trackList.indexOf(this) === -1) artist?.trackList.push(this);
    if (album?.trackList.indexOf(this) === -1) album?.trackList.push(this);
  }

  static build(name?: string, time?: number, explicit?: boolean, album?: Album, artist?: Artist): Track {
    return name && time && explicit !== undefined
      ? new Track(name, time, explicit, album, artist)
      : new Track(`Track`, 20, false);
  }

  public assignArtist(artist: Artist): void {
    this.artist = artist;
    if (artist.trackList.indexOf(this) === -1) artist.trackList.push(this);
  }

  public assignAlbum(album: Album): void {
    this.album = album;
    if (album.trackList.indexOf(this) === -1) album.trackList.push(this);
  }
}
