import { Track } from "./Track";
import { Album } from "./Album";
import { Artist } from "./Artist";
import { player } from "./Player";

const libraryPlayer = player;
export class Library {

    public tracks!: Track[];
    public albums!: Album[];
    public artists!: Artist[];

    [prop: string]: unknown;

    static _instance: Library;

    public currentTrackIndex!: number;
    public secondsPlayed!: number;

    public nextButton!: boolean;
    public previousButton!: boolean;
    public pauseButton!: boolean;

    constructor() {
        if (Library._instance) {
            return Library._instance;
        }
        Library._instance = this;

        this.tracks = [];
        this.albums = [];
        this.artists = [];

        this.currentTrackIndex = 0;
        this.secondsPlayed = 0;

        this.nextButton = false;
        this.previousButton = false;
        this.pauseButton = false;
    }

    public play() {
        libraryPlayer.emit('play', this);
    }

    public next() {
        libraryPlayer.emit('next', this);
    }

    public previous() {
        libraryPlayer.emit('previous', this);
    }

    public pause() {
        libraryPlayer.emit('pause', this);
    }

    public add(...items: unknown[]) {
        for (let i: number = 0; i < items.length; i++) {
            try {
                if (Array.isArray(items[i])) {
                    (items[i] as (Track | Album | Artist)[]).forEach((element, index) => {
                        try {
                            if (element instanceof Track) {
                                if (this.tracks.indexOf(element as Track) === -1) this.tracks.push(element)
                                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
                            } else if (element instanceof Album) {
                                if (this.albums.indexOf(element as Album) === -1) this.albums.push(element)
                                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
                            } else if (element instanceof Artist) {
                                if (this.artists.indexOf(element as Artist) === -1) this.artists.push(element)
                                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
                            } else throw new Error(`Please enter valid and existing object name for element ${index + 1} of the array ${i + 1}!`);
                        } catch (error) {
                            console.log(error.message);
                        }
                    });
                } else
                    if (items[i] instanceof Track) {
                        if (this.tracks.indexOf(items[i] as Track) === -1) this.tracks.push(items[i] as Track)
                        else throw new Error(`The element ${i + 1} is already in the Library!`);
                    } else if (items[i] instanceof Album) {
                        if (this.albums.indexOf(items[i] as Album) === -1) this.albums.push(items[i] as Album)
                        else throw new Error(`The element ${i + 1} is already in the Library!`);
                    } else if (items[i] instanceof Artist) {
                        if (this.artists.indexOf(items[i] as Artist) === -1) this.artists.push(items[i] as Artist)
                        else throw new Error(`The element ${i + 1} is already in the Library!`);
                    } else throw new Error('Please enter valid and existing object name!');
            } catch (error) {
                console.log(error.message);
            }
        }
    }

    public delete(...items: unknown[]) {
        for (let i: number = 0; i < items.length; i++) {
            let itemIndex: number;
            try {
                if (Array.isArray(items[i])) {
                    (items[i] as (Track | Album | Artist)[]).forEach((element, index) => {
                        try {
                            if (element instanceof Track && this.tracks.indexOf(element as Track) !== -1) {
                                itemIndex = this.tracks.indexOf(element as Track)
                                this.tracks.splice(itemIndex, 1);
                            } else if (element instanceof Album && this.albums.indexOf(element as Album) !== -1) {
                                itemIndex = this.albums.indexOf(element as Album)
                                this.albums.splice(itemIndex, 1);
                            } else if (element instanceof Artist && this.artists.indexOf(element as Artist) !== -1) {
                                itemIndex = this.artists.indexOf(element as Artist)
                                this.artists.splice(itemIndex, 1);
                            } else throw new Error(`The Library doesn't contain item ${index + 1} of the array ${i + 1}! There is nothing to delete!`);
                        } catch (error) {
                            console.log(error.message);
                        }
                    });
                } else
                    if (items[i] instanceof Track && this.tracks.indexOf(items[i] as Track) !== -1) {
                        itemIndex = this.tracks.indexOf(items[i] as Track)
                        this.tracks.splice(itemIndex, 1);
                    } else if (items[i] instanceof Album && this.albums.indexOf(items[i] as Album) !== -1) {
                        itemIndex = this.albums.indexOf(items[i] as Album)
                        this.albums.splice(itemIndex, 1);
                    } else if (items[i] instanceof Artist && this.artists.indexOf(items[i] as Artist) !== -1) {
                        itemIndex = this.artists.indexOf(items[i] as Artist)
                        this.artists.splice(itemIndex, 1);
                    } else throw new Error(`The Library doesn't contain item ${i + 1} you have passed! There is nothing to delete!`);
            } catch (error) {
                console.log(error.message);
            }
        }
    }

    public update<T extends [Track | Artist | Album, string, unknown]>(...args: T) {
        try {
            if (args[0][args[1]]) {

                if (this.tracks.indexOf(args[0] as Track) !== -1 || this.albums.indexOf(args[0] as Album) !== -1 || this.artists.indexOf(args[0] as Artist) !== -1) {
                    args[0][args[1]] = args[2];
                } else throw new Error(`The Library doesn't contain item you have passed! There is nothing to update!`);

            } else throw new Error(`Please check the property name and new value type! There is nothing to update!`);
        } catch (error) {
            console.log(error.message);
        }
    }

    public display() {
        interface Result {
            tracks: string[],
            albums: string[],
            artists: string[],
            [prop: string]: string[];
        }

        let result: Result = {
            tracks: [],
            albums: [],
            artists: [],
        };

        for (let prop in result) {
            if (this[prop]) {
                (this[prop] as (Artist | Album | Track)[]).map(item => {
                    result[prop].push((item as Artist).artistName ? (item as Artist).artistName : (item as Track).trackName ? (item as Track).trackName : (item as Album).albumName);
                });
            }
        }

        console.log(result);
    }

    public search(itemName: string) {
        return new Promise((resolve, reject) => {
            interface searchResult {
                [prop: string]: string[];
            }

            let searchResult: searchResult = {};
            let categoryFilter;

            for (let prop in this) {
                if (prop === `tracks`) {
                    categoryFilter = (this[prop] as Track[]).map(item => item.trackName).filter(item => item.includes(itemName))
                    if (categoryFilter.length) searchResult[prop] = categoryFilter;
                } else if (prop === `albums`) {
                    categoryFilter = (this[prop] as Album[]).map(item => item.albumName).filter(item => item.includes(itemName))
                    if (categoryFilter.length) searchResult[prop] = categoryFilter;
                } else if (prop === `artists`) {
                    categoryFilter = (this[prop] as Artist[]).map(item => item.artistName).filter(item => item.includes(itemName))
                    if (categoryFilter.length) searchResult[prop] = categoryFilter;
                }
            }

            setTimeout(() => {
                if (Object.keys(searchResult).length > 0) {
                    resolve(console.log(searchResult));
                } else {
                    reject(new Error(`No results found!`).message);
                }
            }, 300);

        }).catch(console.log);
    }
}

type OneArgumentType = `search`;
type ThreeArgumentsType = `update`;
type ManyArgumentsType = `add` | `delete`;

function isOneArgumentType(item: keyof Library): item is OneArgumentType {
    return item === `search`;
}

function isThreeArgumentsType(item: keyof Library): item is ThreeArgumentsType {
    return item === `update`;
}

function isManyArgumentsType(item: keyof Library): item is ManyArgumentsType {
    return item === `add` || item === `delete`;
}
interface Command {
    execute(action: string, ...args: unknown[]): void;
}
export class LibraryCommand implements Command {

    constructor(private readonly library: Library) {}

    public execute<A extends [Artist | Track | Album, string, unknown] | unknown[]>(action: keyof Library, ...items: A) {
        try {
        if ( isOneArgumentType(action) ) this.library[ action ].call(this.library, items[0] as string)
        else if ( isThreeArgumentsType(action) ) this.library[ action ].call(this.library, ...items as [Artist | Track | Album, string, unknown])
        else if ( isManyArgumentsType(action) ) this.library[ action ].call(this.library, ...items)
        else (this.library[ action ] as () => void).call(this.library);
        } catch(error) {
            console.log(`Invalid command or parameter passed!`);
        }
    };
    
}