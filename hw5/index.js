const Artist = require('./Artist').Artist;
const Album = require('./Album').Album;
const Track = require('./Track').Track;
const Library = require('./Library').Library;
const file = require('./parser').file;

console.log('======Calculator Task======');

setTimeout(() => console.log('\n' + '===Calculator Task Unit Testing==='), 7000);
setTimeout(() => console.log(`To perform unit testing fot Calculator task you need to run "npm run test ./hw5/parser_tests.js" from the console window.`), 7000);

// for covenience 8 seconds delay is implemented for Library task...

const Godzilla = new Track('Godzilla', 5, true);
const Darkness = new Track('Darkness', 4, true);
const LoseYourself = new Track('Lose Yourself', 4, true);

const newLibrary = new Library();

setTimeout(() => console.log(`\n======Music Library Task======`), 8000);

setTimeout(() => console.log(`\n===Let's create our tracks, add them to the Library and enjoy listening!===`), 8000);

// adding tracks to the Library and play the list without switching...

newLibrary.add(Darkness, Godzilla, LoseYourself);

setTimeout(() => newLibrary.play(), 8000);

// playing the list switching tracks back and forth (don't forget about Error handling)...
setTimeout(() => newLibrary.next(), 10000);
setTimeout(() => newLibrary.previous(), 11000);
setTimeout(() => newLibrary.previous(), 13000);

// pausing track playing...
setTimeout(() => newLibrary.pause(), 21000);

// ...and playing again from the place we have stopped in 2 seconds after pause...
setTimeout(() => newLibrary.play(), 23000);
setTimeout(() => newLibrary.next(), 26000);