let task1 = require('./task1').mul;
const task2 = require('./task2').card;
const task3 = require('./task3');

console.log('======task1======');

console.log('\n' + '===Multiply Each Next Number With The Previous===');
console.log(`Start with 2: ${task1(2)}`);
console.log(`The next is 5: ${task1(5)}`);
console.log(`The next is 11: ${task1(11)}`);
console.log(`And finally 3: ${task1(3)}`);

console.log(`\n` + '======task2======');

console.log('\n' + '===Get Card User And Balance===');
console.log(`${task2.name}: ${task2.balance}`);

console.log('\n' + '===Get Card Balance===');
console.log(task2.getBalance());

console.log('\n' + '===Add Money To Card Balance===');
console.log(task2.addToBalance(300));

console.log('\n' + '===Reduce Card Balance===');
console.log(task2.reduceBalance(500));

console.log('\n' + '===Get Card Balance In Currency===');
console.log(task2.getBalanceInCurrency(2.345));

console.log('\n' + '===Change Card Balance Manually===');
let newBalance = 2500;
task2.balance = newBalance;
console.log(`An error happenned!
Unfortunately, you can not change card balance to ${newBalance} BYN manually.
Your current balance is ${task2.balance} BYN.`);

console.log('\n' + '===Task #2 Unut Testing===');
console.log(`To perform unit testing fot task #2 you need to run "npm run test ./hw3/task2_tests.js" from the console window.`);

console.log(`\n` + '======task3======');

let Belarusneft = new task3.GasolineStation();
let Lukoil = new task3.GasolineStation();
let A100 = new task3.GasolineStation();
let Belarusneft2 = new task3.GasolineStation();
let Rosneftekhim = new task3.GasolineStation();

let stations = [
    {station: Belarusneft, fuelAmount: 3, distanceFromStart: 20},
    {station: Lukoil, fuelAmount: 3, distanceFromStart: 70},
    {station: A100, fuelAmount: 4, distanceFromStart: 100},
    {station: Belarusneft2, fuelAmount: 3, distanceFromStart: 150},
    {station: Rosneftekhim, fuelAmount: 4, distanceFromStart: 170},
];

task3.Car.passDistance(stations, 200);