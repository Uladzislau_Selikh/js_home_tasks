const repl = require('repl');
const hotkeys = require('node-hotkeys');

import { Library, LibraryCommand } from './Library';
import { Album } from './Album';
import { Artist } from './Artist';
import { Track } from './Track';

const Godzilla = new Track('Godzilla', 8, true);
const Darkness = new Track('Darkness', 8, true);
const MusicToBeMurderedBy = new Album('Music To Be Murdered By', new Date('05/11/2020'), [Darkness]);
const Eminem = new Artist('Eminem', new Date('12/11/1994'), undefined, [MusicToBeMurderedBy]);
const TillLindemann = new Artist('Till Lindemann', new Date('12/11/1976'));

const newLibrary = new Library();
const newLibraryCommand = new LibraryCommand(newLibrary);

Godzilla.assignArtist(Eminem);
Godzilla.assignAlbum(MusicToBeMurderedBy);
Darkness.assignArtist(Eminem);

const LoseYourself = new Track('Lose Yourself', 5, true);
const EightMile = new Album('Eight Mile', new Date('05/11/1995'));

LoseYourself.assignArtist(Eminem);
LoseYourself.assignAlbum(EightMile);
Eminem.addAlbums([EightMile]);

console.log(`\n======Music Library Task======`);

console.log(`\n===Let's verify that Command Executor works correctly!===`);

newLibraryCommand.execute(`add`, Darkness, Godzilla, Eminem, MusicToBeMurderedBy);

console.log(newLibraryCommand.execute(`display`));

newLibraryCommand.execute(`update`, Eminem, `artistName`, `Marshall`);
newLibraryCommand.execute(`add`, LoseYourself, TillLindemann);
newLibraryCommand.execute(`delete`, TillLindemann, EightMile);

console.log(newLibraryCommand.execute(`display`));
newLibraryCommand.execute(`shuffle`);
newLibraryCommand.execute(`search`, `se`);

console.log(`\n===Enjoy the music with our player!===\n`);

const replServer = repl.start({
  prompt: `Enter ".play", ".next", ".previous" or ".pause"...> `
});

replServer.defineCommand('play', () => {
  newLibraryCommand.execute('play');
  replServer.displayPrompt;
});

replServer.defineCommand('pause', () => {
  newLibraryCommand.execute('pause');
  replServer.displayPrompt;
});

replServer.defineCommand('next', () => {
  newLibraryCommand.execute('next');
  replServer.displayPrompt;
});

replServer.defineCommand('previous', () => {
  newLibraryCommand.execute('previous');
  replServer.displayPrompt;
});

hotkeys.on({
  hotkeys: '1',
  matchAllModifiers: true,
  callback() {
    newLibraryCommand.execute('play');
  }
});

hotkeys.on({
  hotkeys: '2',
  matchAllModifiers: true,
  callback() {
    newLibraryCommand.execute('next');
  }
});

hotkeys.on({
  hotkeys: '3',
  matchAllModifiers: true,
  callback() {
    newLibraryCommand.execute('previous');
  }
});

hotkeys.on({
  hotkeys: '4',
  matchAllModifiers: true,
  callback() {
    newLibraryCommand.execute('pause');
  }
});
