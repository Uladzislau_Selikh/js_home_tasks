const webdriver = require(`selenium-webdriver`),
    By = webdriver.By,
    until = webdriver.until,
    Key = webdriver.Key
const chrome = require(`selenium-webdriver/chrome`);
const path = require(`chromedriver`).path;

const service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);

const driver = new webdriver.Builder()
    .withCapabilities(webdriver.Capabilities.chrome())
    .build();
(async function openTSHandBook() {
    try {
        await driver.manage().window().maximize();
        await driver.get(`https://www.typescriptlang.org/`);
        await driver.findElement(By.css(`nav a[href="/docs/handbook/intro.html"]`)).click();
        await driver.wait(until.titleContains(`The TypeScript Handbook`), 2000);
    } catch (error) {
        console.log(error.message)
    }

    try {
        await driver.findElement(By.xpath(`//a[@href="/docs/handbook/functions.html"]`)).click();
        await driver.wait(until.urlIs(`https://www.typescriptlang.org/docs/handbook/functions.html`), 2000);

        await driver.sleep(2000);
        await driver.navigate().back();

        await driver.sleep(2000);
        await driver.findElement(By.css(`aside #like-button`)).click();
    } catch (error) {
        console.log(error.message)
    } finally {
        await driver.quit()
    }
})();