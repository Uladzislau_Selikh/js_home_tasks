function Multiply() {

    this.result = 1;
    this.execute = function(number) {
        this.result *= number;
        return this.result;
    };

}

let init = new Multiply();

let mul = value => init.execute(value);

module.exports = {
    mul
}