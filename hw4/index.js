const credit = require('./Card').credit;
const debit = require('./Card').debit;
const Pizza = require('./PIZZA').Pizza;
const Artist = require('./Artist').Artist;
const Album = require('./Album').Album;
const Track = require('./Track').Track;
const Library = require('./Library').Library;

console.log('======Card Task======');

console.log('\n' + '===Get Card Balance===');
console.log(`Debit card:\n${debit.getBalance}\n`);
console.log(`Credit card:\n${credit.getBalance}`);

console.log('\n' + '===Reduce Card Balance===');
console.log(`Debit card:\n${debit.reduceBalance(500)}\n`);
console.log(`Credit card:\n${credit.reduceBalance(300)}`);

console.log('\n' + '===Reduce Card Balance By The Amount More Than The Current Balance===');
console.log(`Debit card:\n${debit.reduceBalance(800)}\n`);
console.log(`Credit card:\n${credit.reduceBalance(800)}`);

console.log('\n' + '===Add Money To Card Balance===');
console.log(`Debit card:\n${debit.addToBalance(100)}\n`);
console.log(`Credit card:\n${credit.addToBalance(800)}`);

console.log('\n' + '===Get Card Balance In Currency===');
console.log(`Debit card:\n${debit.getBalanceInCurrency(2.345)}\n`);
console.log(`Credit card:\n${credit.getBalanceInCurrency(2.456)}`);

console.log('\n' + '===Change Card Balance Manually===');
let newBalance = 2500;
debit.balance = newBalance;
console.log(`Debit card: An error happened!
Unfortunately, you can not change card balance to ${newBalance} BYN manually.
Your current balance is ${debit.balance} BYN.\n`);
credit.balance = newBalance;
console.log(`Credit card: An error happened!
Unfortunately, you can not change card balance to ${newBalance} BYN manually.
Your current balance is ${credit.balance} BYN.`);

console.log('\n' + '===Card Task Unit Testing===');
console.log(`To perform unit testing fot Card task you need to run "npm run test ./hw4/Card_tests.js" from the console window.`);

console.log(`\n` + '======Pizza Task======');

console.log(`\n===Let's create a small pizza!===`);
const myPizza = new Pizza(Pizza.sizes.small);
myPizza.getCostAndCalories;

console.log(`\n===Let's add salad, meat and cheese to our pizza!===`);
myPizza.addToppings(Pizza.toppings.salad);
myPizza.addToppings(Pizza.toppings.cheese);
myPizza.addToppings(Pizza.toppings.meat);
myPizza.getCostAndCalories;

console.log(`\n===Let's remove salami and cheese from our pizza!===`);
myPizza.removeToppings(Pizza.toppings.salami);
console.log(`Ooops!`);
myPizza.removeToppings(Pizza.toppings.cheese);
myPizza.getCostAndCalories;

console.log(`\n===Let's change the size of our pizza!===`);
myPizza.switchSize(Pizza.sizes.small);
console.log(`Ooops!`);
myPizza.switchSize(Pizza.sizes.big);
myPizza.getCostAndCalories;

console.log(`\n===Can I consider my pizza as vegan?===`);
myPizza.isVegan;
console.log(`\n===And if I remove meat?===`);
myPizza.removeToppings(Pizza.toppings.meat);
myPizza.isVegan;
console.log(`\n===So... Let's add salami instead of meat!===`);
myPizza.addToppings(Pizza.toppings.salami);
myPizza.getCostAndCalories;

console.log(`\n===Bon appetit!===`);
myPizza.getCostAndCalories;
console.log(`It has the following toppings: ${myPizza.toppings.filter((item, index) => {
    return myPizza.toppings.indexOf(item) === index;
}).join(`, `)}!`);
console.log(`Its size is ${myPizza.sizes}!`);

console.log(`\n======Music Library Task======`);

console.log(`\n===Let's create our items, add some to the Library and display its content!===`);

const Godzilla = new Track('Godzilla', 180, true);
const Darkness = new Track('Darkness', 300, true);
const MusicToBeMurderedBy = new Album('Music To Be Murdered By', new Date('05/11/2020'), [Darkness]);
const Eminem = new Artist('Eminem', new Date('12/11/1994'), [Darkness]);
const TillLindemann = new Artist('Till Lindemann', new Date('12/11/1976'));

Godzilla.assignArtist(Eminem);
Godzilla.assisnAlbum(MusicToBeMurderedBy);

Darkness.assignArtist(Eminem);
Darkness.assisnAlbum(MusicToBeMurderedBy);

MusicToBeMurderedBy.assignArtist(Eminem);
MusicToBeMurderedBy.addTracks([Godzilla]);

const LoseYourself = new Track('Lose Yourself', 200, true);
const EightMile = new Album('Eight Mile', new Date('05/11/1995'));

LoseYourself.assignArtist(Eminem);
LoseYourself.assisnAlbum(EightMile);

Eminem.addTracks([LoseYourself, Godzilla]);
Eminem.addAlbums([EightMile, MusicToBeMurderedBy]);
EightMile.assignArtist(Eminem);
EightMile.addTracks([LoseYourself]);

const newLibrary = new Library();

// adding items to the Library and not forgetting about Error handling

newLibrary.add(MusicToBeMurderedBy);
newLibrary.add(LoseYourself, Darkness, [EightMile, MusicToBeMurderedBy], Eminem);
newLibrary.add(Darkness, TillLindemann, Godzilla);

newLibrary.display();

console.log(`\n===Now I want to delete some items from the Library!===`);

// deleting items from the Library and not forgetting about Error handling

newLibrary.delete(LoseYourself);
newLibrary.delete(EightMile, Darkness, LoseYourself, 'MusicToBeMurderedBy');
newLibrary.delete(TillLindemann);

newLibrary.display();

console.log(`\n===And if I need to update some records in the Library...===`);

// updating Library items and not forgetting about Error handling

newLibrary.update(Godzilla, 'trackName', 'Godjirra');
newLibrary.update(Eminem, 'artistName', 'EMINEM');
newLibrary.update(LoseYourself, 'trackName', 'LYS');

newLibrary.display();