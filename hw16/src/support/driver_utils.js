const { browser } = require("protractor");
const { defaultTimeoutMs } = require("./constants");
const randomstring = require("randomstring");

function generateAlphabeticString(length) {
    return randomstring.generate({ length,
        charset: "alphabetic" });
}

async function highlightElement(element) {
    await browser.executeScript(
        `const realBorder = arguments[0].style.border;
        arguments[0].style.border='3px solid red';
        setTimeout(() => {
            arguments[0].style.border=realBorder;
        }, 2000)`,
        element
        );
}

class DriverUtils {
    static scrollToElement = async element => {
        await browser.controlFlow().execute(async () => {
            await browser.executeScript('arguments[0].scrollIntoView(true)', element);
        })
        await highlightElement(element);
        await browser.sleep(1000)
    }

    static clickElement = async function(element) {
        await DriverUtils.scrollToElement(element);
        await element.click();
    };

    static getElementInnerText = async element => {
        await DriverUtils.scrollToElement(element);

        return element.getText();
    };

    static enterText = async (element, text) => {
        await DriverUtils.scrollToElement(element);
        await element.sendKeys(text);
    };
}

module.exports = {
    DriverUtils,
    highlightElement,
    generateAlphabeticString
}
