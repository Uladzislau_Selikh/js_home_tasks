globalThis.arrToObject = function (initArr) {
    if (initArr.length % 2 === 1) {
        initArr.push(null);
    }
    let newArr = initArr;
    let newObj = {};
    let key;
    let value;
    newArr.forEach(function pushItem(item, index) {
        if (index % 2 === 0) {
        key = item;
        }
        else {
        value = item;
        newObj[key] = value;
        }
    });
    newObj['length'] = newArr.length / 2;
    console.log(newObj);
}