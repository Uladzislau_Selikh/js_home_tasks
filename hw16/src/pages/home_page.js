const { by, element } = require("protractor");
const BasePage = require("./base_page");
const { DriverUtils } = require("../support/driver_utils");

class WelcomePage extends BasePage {
    constructor() {
        super();
    }

    scrollToNavigationLinks = async () => {
        await DriverUtils.scrollToElement(element(by.css(`nav.home-nav-links`)));
    }

    scrollToHeader = async () => {
        await DriverUtils.scrollToElement(element(by.xpath(`//a[@href='/login']`)));
    }

    getNavigationLinks = () => element.all(by.className(`home-nav-item`))

    clickSignInButton = async () => {
        const button = await element(by.xpath(`//a[@href='/login']`))
        await DriverUtils.clickElement(button);
    }
}

module.exports = new WelcomePage()
