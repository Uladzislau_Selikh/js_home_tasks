exports.config = {

    directConnect: true,

    framework: 'mocha',

    specs: [
        '../specs/*.spec.js'
    ],

    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: ["--incognito", "--no-sandbox", "--disable-web-security", "--allow-running-insecure-content", "--disable-gpu", "--start-maximized", "disable-extensions", "--disable-infobars"]
        },
    },

    baseUrl: 'https://github.com/',

    onPrepare: async function(){
        await browser.waitForAngularEnabled(false);
        await browser.manage().window().maximize();
    },

    mochaOpts: {
        reporter: 'spec',
        timeout: 70000
    }
};