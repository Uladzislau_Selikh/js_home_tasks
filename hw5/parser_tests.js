/**
 * "input.txt" file content:
 * 2 2 +
 * 4 2 /
 * 1234.12 4564 -
 * 3 4 *
 * 16 5 %
 * 345 789 +
 */

const file = require('./parser').file;
const assert = require('assert');
const sinon = require('sinon');
const calculatorEvents = require('./calculator-events').calculatorEvents;
const calculator = require('./calculator-events').newCalculator;


describe('File Parser Unit Testing', function () {

    it('As a tester I want to ensure that the result event is called after parsing the line!', function () {
        let spyResult = sinon.spy();
        calculatorEvents.on('result', () => spyResult());
        setTimeout(() => sinon.assert.callCount(spyResult, 6), 7000);
    });

    it('As a tester I want to ensure that the sum event is called 2 times after parsing the file!', function () {
        let spySum = sinon.spy();
        calculatorEvents.on('sum', () => spySum());
        setTimeout(() => sinon.assert.callCount(spySum, 2), 7000);
    });

    
    it('As a tester I want to ensure that the divide event is called 1 time after parsing the file!', function () {
        let spyDiv = sinon.spy();
        calculatorEvents.on('divide', () => spyDiv());
        setTimeout(() => sinon.assert.callCount(spyDiv, 1), 7000);
    });

    it('As a tester I want to ensure that the deduct event is called 1 time after parsing the file!', function () {
        let spyDeduct = sinon.spy();
        calculatorEvents.on('deduct', () => spyDeduct());
        setTimeout(() => sinon.assert.callCount(spyDeduct, 1), 7000);
    });

    it('As a tester I want to ensure that the remainer event is called 1 time after parsing the file!', function () {
        let spyRemain = sinon.spy();
        calculatorEvents.on('remainer', () => spyRemain());
        setTimeout(() => sinon.assert.callCount(spyRemain, 1), 7000);
    });

    it('As a tester I want to ensure that the multiply event is called 1 time after parsing the file!', function () {
        let spyMultiply = sinon.spy();
        calculatorEvents.on('multiply', () => spyMultiply());
        setTimeout(() => sinon.assert.callCount(spyMultiply, 1), 7000);
    });

    it('As a tester I want to ensure that the result of the operation is correct!', function () {
        let spyMultiply = sinon.spy();
        calculatorEvents.on('multiply', () => spyMultiply());
        setTimeout(() => assert.strictEqual(calculator.result, 2), 2000);
    });

})
