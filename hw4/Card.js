class Card {

    constructor(name = `User`, balance=0) {
        this.balance = balance;
        this.name = name;
        Object.defineProperty(this, 'balance', {
            writable: false,
        });
    
    }
  
    get getBalance() {
        return `Dear ${this.name},\nYour current balance is ${this.balance} BYN!`
    }
  
    addToBalance(addition) {
        Object.defineProperty(this, 'balance', {
            writable: true,
        });
  
        this.balance += addition;
  
        Object.defineProperty(this, 'balance', {
            writable: false,
        });
  
        return `Dear ${this.name},\nYou have added ${addition} BYN to your card.\nThe current balance is ${this.balance} BYN.`;
    }
  
  reduceBalance(reducer) {
    Object.defineProperty(this, 'balance', {
        writable: true,
    });
  
    this.balance -= reducer;
  
    Object.defineProperty(this, 'balance', {
        writable: false,
    });
  
    return `Dear ${this.name},\nYour balance is reduced by ${reducer} BYN.\nThe current balance is ${this.balance} BYN.`;
  }

  getBalanceInCurrency(exchRate) {
    return `Dear ${this.name},\nYour balance in currency calculated with ${exchRate} exchange rate is:\n${Math.round(this.balance / exchRate)} USD.`;
}

  }
  
  class CreditCard extends Card {};

  class DebitCard extends Card {
    reduceBalance(reducer) {
        try {
  
        if (this.balance < reducer) {
            throw new Error(`Not enough cash! Please enter another amount!`);
        }
  
        Object.defineProperty(this, 'balance', {
            writable: true,
        });
  
        this.balance -= reducer;
    
        Object.defineProperty(this, 'balance', {
            writable: false,
        });
    
        return `Dear ${this.name},\nYour balance is reduced by ${reducer} BYN.\nThe current balance is ${this.balance} BYN.`;
    } catch (error) {
        return error.message;
    }
  }
  }
  
  const credit = new CreditCard('Uladzislau', 1000);
  const debit = new DebitCard('Vitali', 1200);
  
  module.exports = {
    credit,
    debit
  }