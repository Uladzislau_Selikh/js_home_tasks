const { browser } = require("protractor");
const { defaultTimeoutMs } = require("./constants");
const EC = browser.ExpectedConditions;
const randomstring = require("randomstring");

function generateAlphabeticString(length) {
    // eslint-disable-next-line object-property-newline
    return randomstring.generate({ length, charset: "alphabetic" });
}
class DriverUtils {
    static clickElement = async element => {
        await browser.wait(EC.elementToBeClickable(element), defaultTimeoutMs);
        await element.click();
    };

    static getElementInnerText = async element => {
        await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);

        return element.getText();
    };

    static enterText = async (element, text) => {
        await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);
        await element.sendKeys(text);
    };

    static scrollToElement = async element => {
        await browser.actions().mouseMove(element).perform();
    };
}

module.exports = {
    DriverUtils,
    generateAlphabeticString
}
