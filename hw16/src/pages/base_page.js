const { browser } = require("protractor");

class BasePage {

    get getPageTitle() {
        return browser.getTitle();
    }

    get getCurrentUrl() {
        return browser.getCurrentUrl();
    }
}

module.exports = BasePage;
