const task1 = require('./task1');
const task2 = require('./task2');

console.log('======task1======');
globalThis.arrToObject(['1', 1, 'a', {hello: 'world'}, 'qwe']);

console.log('\n' + '======task2======');

console.log('\n' + '===MatchedCities===');
task2.citiesMatched('Grodno');

console.log('\n' + '===Age===');
task2.lessOrSameAge(20);

console.log('\n' + '===SortByName===');
task2.sortByName();

console.log('\n' + '===filterByMarriedStatus===');
task2.filterByMarriedStatus();

console.log('\n' + '===AgeAndSexMatched===');
task2.ageAndSexMatched(23, 'Female');

console.log('\n' + '===AddNewStudent===');
task2.addNewStudent({
    name: 'Vlad Selikh',
    age: 23,
    sex: 'Male',
    isMarried: false,
    city: 'Mogilev',
});

console.log('\n' + '===UniqueCities===');
task2.uniqueCities();