class Artist {
    constructor(name, birth, tracks, albums) {
      this.artistName = name;
      this.artistBirth = birth;
      this.albums = albums || [];
      this.trackList = tracks || [];
    }

    addTracks(tracks) {
      tracks.forEach(element => {
        this.trackList.push(element);
      });
    }

    addAlbums(albums) {
      albums.forEach(element => {
        this.albums.push(element);
      });
    }
  }

  module.exports = {
    Artist
}