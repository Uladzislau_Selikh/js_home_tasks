const fs = require('fs');
const readline = require('readline');
const calculatorEvents = require('./calculator-events').calculatorEvents;

const file = readline.createInterface({
    input: fs.createReadStream('./hw5/input.txt'),
    output: process.stdout,
    terminal: false
});

let stringsArray;
let numbersArray;
let timeout = 1000;
let operation;

file.on('line', (line) => {
    stringsArray = line.split(' ');

    numbersArray = stringsArray.map(item => +item ? +item : item);

    function displayResult(delay, numbersArray) {
        setTimeout(() => {
            switch (numbersArray[2]) {
                case '+':
                    operation = 'sum';
                    break
                case '-':
                    operation = 'deduct';
                    break
                case '*':
                    operation = 'multiply';
                    break
                case '/':
                    operation = 'divide';
                    break
                case '%':
                    operation = 'remainer';
                    break
            }
            calculatorEvents.emit(operation, [numbersArray[0], numbersArray[1]]);
            calculatorEvents.emit('result');
        }, delay);
    }

    displayResult(timeout, numbersArray);
    
    timeout += 1000;
});

module.exports = { file }