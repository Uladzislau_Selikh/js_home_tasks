const webdriver = require(`selenium-webdriver`),
By = webdriver.By,
until = webdriver.until,
Key = webdriver.Key
const chrome = require(`selenium-webdriver/chrome`);
const path = require(`chromedriver`).path;

const service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);

const driver = new webdriver.Builder()
.withCapabilities(webdriver.Capabilities.chrome())
.build();
(async function handleJQueryPage() {
    try{
        await driver.manage().window().maximize();
        await driver.get(`https://jqueryui.com/`);

        await driver.findElement(By.xpath(`//input[@name="s"]`)).sendKeys('selectable', Key.RETURN);
        await driver.findElement(By.css(`header a[href="//jqueryui.com/selectable/"]`)).click();
        await driver.wait(until.titleContains(`Selectable | jQuery UI`), 2000);

        const selectableFrame = await driver.findElement(By.xpath(`//iframe[@class="demo-frame"]`));
        await driver.executeScript("arguments[0].scrollIntoView(true)", selectableFrame);
        await driver.switchTo().frame(selectableFrame);

        const element1 = await driver.findElement(By.css(`li:nth-child(1)`));
        const element5 = await driver.findElement(By.css(`li:nth-child(5)`));
        const element6 = await driver.findElement(By.css(`li:nth-child(6)`));

        await driver.actions().keyDown(Key.CONTROL).click(element1).click(element5).click(element6).keyUp(Key.SHIFT).perform();
        await driver.sleep(2000);
        await driver.switchTo().parentFrame();

        await driver.findElement(By.css(`aside a[href="https://jqueryui.com/resizable/"]`)).click();

        const winHandles = await driver.getAllWindowHandles();
        await driver.close();
        await driver.switchTo().window(winHandles[1]);
        await driver.wait(until.titleContains(`Resizable | jQuery UI`), 2000);
        await driver.sleep(1000);

        const resizableFrame = await driver.findElement(By.xpath(`//iframe[@class="demo-frame"]`));
        await driver.executeScript("arguments[0].scrollIntoView(true)", resizableFrame);
        await driver.switchTo().frame(resizableFrame);

        const uiHandleSE = await driver.findElement(By.css(`div.ui-resizable-handle.ui-resizable-se`));
        const uiHandleS = await driver.findElement(By.css(`div.ui-resizable-handle.ui-resizable-s`));
        const uiHandleE = await driver.findElement(By.css(`div.ui-resizable-handle.ui-resizable-e`));

        await driver.actions().dragAndDrop(uiHandleSE, {x: 120, y: 120}).perform();
        await driver.sleep(1000);

        await driver.actions().dragAndDrop(uiHandleS, {x: 0, y: 120}).perform();
        await driver.sleep(1000);

        await driver.actions().dragAndDrop(uiHandleE, {x: 120, y: 0}).perform();
        await driver.sleep(1000);

        await driver.switchTo().parentFrame();
        await driver.sleep(1000);

        await driver.findElement(By.xpath('//a[text()[contains(.,"Sortable")]]')).click();

        const winUpdatedHandles = await driver.getAllWindowHandles();
        await driver.close();
        await driver.switchTo().window(winUpdatedHandles[1]);
        await driver.wait(until.titleContains(`Sortable | jQuery UI`), 2000);
        await driver.sleep(1000);

        const sortableFrame = await driver.findElement(By.xpath(`//iframe[@class="demo-frame"]`));
        await driver.executeScript("arguments[0].scrollIntoView(true)", sortableFrame);
        await driver.switchTo().frame(sortableFrame);

        const item2 = await driver.findElement(By.css(`li:nth-child(2)`));
        await driver.executeScript("arguments[0].style.border='3px solid red'", item2);

        const item5 = await driver.findElement(By.css(`li:nth-child(5)`));
        await driver.executeScript("arguments[0].style.border='3px solid red'", item5);
        await driver.sleep(1000);

        await driver.actions({async: true}).dragAndDrop(item5, item2).perform();
        await driver.sleep(1000);
    } finally {
        await driver.quit();
    }
})();