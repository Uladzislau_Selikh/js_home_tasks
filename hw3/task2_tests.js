const task2 = require('./task2').card;
let assert = require('assert');

describe('task #2 unit testing', function () {

    it('As a user I want to see my card balance.', function () {
        assert.strictEqual(task2.getBalance(), `Dear Uladzislau_Selikh,\nYour current balance is 1500 BYN.`);
    });

    it('As a user I want to add money to the card and see the balance.', function () {
        assert.strictEqual(task2.addToBalance(100), `Dear Uladzislau_Selikh,\nYou have added 100 BYN to your card.\nThe current balance is 1600 BYN.`);
        assert.strictEqual(task2.addToBalance(200), `Dear Uladzislau_Selikh,\nYou have added 200 BYN to your card.\nThe current balance is 1800 BYN.`);
        assert.strictEqual(task2.addToBalance(500), `Dear Uladzislau_Selikh,\nYou have added 500 BYN to your card.\nThe current balance is 2300 BYN.`);
    });

    it('As a user I want to reduce my card balance and see the resulting balance.', function () {
        assert.strictEqual(task2.reduceBalance(300), `Dear Uladzislau_Selikh,\nYour balance is reduced by 300 BYN.\nThe current balance is 2000 BYN.`);
        assert.strictEqual(task2.reduceBalance(200), `Dear Uladzislau_Selikh,\nYour balance is reduced by 200 BYN.\nThe current balance is 1800 BYN.`);
        assert.strictEqual(task2.reduceBalance(100), `Dear Uladzislau_Selikh,\nYour balance is reduced by 100 BYN.\nThe current balance is 1700 BYN.`);
    });

    it('As a user I want to view my current balance in USD currency.', function () {
        assert.strictEqual(task2.getBalanceInCurrency(1.523), `Dear Uladzislau_Selikh,\nYour balance in currency calculated with 1.523 exchange rate is:\n1116 USD.`);
        assert.strictEqual(task2.getBalanceInCurrency(1.89), `Dear Uladzislau_Selikh,\nYour balance in currency calculated with 1.89 exchange rate is:\n899 USD.`);
        assert.strictEqual(task2.getBalanceInCurrency(2.345), `Dear Uladzislau_Selikh,\nYour balance in currency calculated with 2.345 exchange rate is:\n725 USD.`);
    });

    it('As a user I want to protect my card from manual balance change.', function () {
        let newBalance = 2500;
        task2.balance = newBalance;
        assert.strictEqual(task2.getBalance(), `Dear Uladzislau_Selikh,\nYour current balance is 1700 BYN.`);
        task2.balance = 2500;
        assert.strictEqual(task2.getBalance(), `Dear Uladzislau_Selikh,\nYour current balance is 1700 BYN.`);
    });

});
