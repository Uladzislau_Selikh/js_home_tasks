const {browser} = require("protractor");
const {expect} = require("chai");
const randomstring = require("randomstring");
const {validLogin, validPassword, baseUrl} = require("../utils/constants");
const LoginPage = require("../pages/login_page");

function generateAlphabeticString(length) {
    return randomstring.generate({ length, charset: "alphabetic" });
}

describe("GitHub SignIn Page", () => {

    beforeEach(async () => {
        await browser.get(`${baseUrl}/login`);
    })

    it("Should redirect user to the password reset page", async () => {
        await LoginPage.clickForgotPasswordLink();
        await browser.sleep(2000);
        await LoginPage.getCurrentUrl.then(item => {
            expect(item).to.contain(`https://github.com/password_reset`);
        })
    });

    it("Should display error message after invalid login pass", async () => {
        const invalidLogin = generateAlphabeticString(12);
        await LoginPage.performLogin(invalidLogin, validPassword);
        await browser.sleep(2000);
        const text = await LoginPage.getErrorMessage();
        expect(text).to.be.deep.equal("Incorrect username or password.");
    });

    it("Should display error message after invalid password pass", async () => {
        const invalidPassword = generateAlphabeticString(10);
        await LoginPage.performLogin(validLogin, invalidPassword);
        await browser.sleep(2000);
        const text = await LoginPage.getErrorMessage();
        expect(text).to.be.deep.equal("Incorrect username or password.");
    });

    it("Should redirect user to private cabinet after valid login and password pass", async () => {
        await LoginPage.performLogin(validLogin, validPassword);
        await browser.sleep(2000);
        const pageTitle = await LoginPage.getPageTitle;
        expect(pageTitle).to.be.deep.equal("GitHub");
    });

});