const task1 = require('./task1');
const task2 = require('./task2');
const task3 = require('./task3');

console.log('======task1======');
task1.factorial(13);

console.log('======task2======');
task2.fibonacci(1150000);

console.log('======task3======');
task3.rabbitsCount(6);