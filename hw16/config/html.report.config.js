const reporter = require('cucumber-html-reporter');

const reportOptions = {
    theme: 'bootstrap',
    jsonFile: './hw16/reports',
    output: './hw16/reports/cucumber-html-report.html',
    reportSuiteAsScenarios: true,
    scenarioTimestamp: true,
    launchReport: true,
    metadata: {
        "App Version": "0.3.2",
        "Test Environment": "Mocha",
        "Browser": "Chrome  54.0.2840.98",
        "Platform": "Windows 10",
        "Parallel": "Scenarios",
        "Executed": "Remote"
    }
}

reporter.generate(reportOptions);
