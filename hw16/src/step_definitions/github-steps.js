const { browser, by, element } = require("protractor");
const { expect } = require("chai");
const { generateAlphabeticString, DriverUtils } = require("../support/driver_utils");
const LoginPage = require("../pages/login_page");
const logger = require("../../config/configLogger").logger;
const { Then, setDefaultTimeout } = require(`@cucumber/cucumber`);
const { validLogin, validPassword, baseUrl } = require("../support/constants");
const WelcomePage = require("../pages/home_page");

setDefaultTimeout(240 * 1000);

// eslint-disable-next-line new-cap
Then(/^open web page (.+)$/u, async (webAddress) => {
    await browser.get(webAddress);
    logger.info(`Opening ${webAddress} web page...`);
});

// eslint-disable-next-line new-cap
Then(/^scroll to navigation links$/u, async () => {
    await WelcomePage.scrollToNavigationLinks();
    logger.info(`Scrolling to navigation links...`);
});

// eslint-disable-next-line new-cap
Then(/^scroll to header$/u, async () => {
    await WelcomePage.scrollToHeader();
    logger.info(`Scrolling to header...`);
});

// eslint-disable-next-line new-cap
Then(/^the text of the link (.+) contains (.+)$/u, async (number, linkText) => {
    try {
        const link = await WelcomePage.getNavigationLinks().get(number - 1);
        logger.info(`Verify the link ${number} inner text is ${linkText}...`);
        await DriverUtils.getElementInnerText(link)
            .then(text => {
                expect(text).to.be.deep.equal(linkText);
            })
    } catch (e) {
        logger.error(e.message);
        throw new Error(e)
    }
});

// eslint-disable-next-line new-cap
Then(/^click on Sign In button$/u, async () => {
    try {
        await WelcomePage.clickSignInButton();
        logger.info(`Signing In...`);
    } catch (e) {
        logger.error(e.message);
        throw new Error(e)
    }
});

// eslint-disable-next-line new-cap
Then(/^the user is redirected to sign-in page$/u, async () => {
    try {
        await WelcomePage.getCurrentUrl
            .then((item) => {
                expect(item).to.contain(`${baseUrl}/login`);
            });
        logger.warn(`Going to sign-in page...`);
        await browser.sleep(2000);
    } catch (e) {
        logger.error(e.message);
        throw new Error(e)
    }
});

// eslint-disable-next-line new-cap
Then(/^click on Forgot password link$/u, async () => {
    try {
        await LoginPage.clickForgotPasswordLink();
        await browser.sleep(2000);
    } catch (e) {
        logger.error(e.message);
        throw new Error(e)
    }
});

// eslint-disable-next-line new-cap
Then(/^the user is on the password reset page$/u, async () => {
    try {
        logger.info(`Validating redirection to the password reset page...`);
        await LoginPage.getCurrentUrl.then(item => {
            expect(item).to.contain(`${baseUrl}/password_reset`);
        })
    } catch (e) {
        logger.error(e.message);
        throw new Error(e)
    }
});
// eslint-disable-next-line new-cap
Then(/^log in with invalid username of (.+) symbols length$/u, async (length) => {
    const invalidLogin = generateAlphabeticString(length);
    await LoginPage.performLogin(invalidLogin, validPassword);
    await browser.sleep(2000);
});

// eslint-disable-next-line new-cap
Then(/^log in with invalid password of (.+) symbols length$/u, async (length) => {
    const invalidPassword = generateAlphabeticString(length);
    await LoginPage.performLogin(validLogin, invalidPassword);
    await browser.sleep(2000);
});

// eslint-disable-next-line new-cap
Then(/^log in with valid credentials$/u, async () => {
    await LoginPage.performLogin(validLogin, validPassword);
    await browser.sleep(2000);
});

// eslint-disable-next-line new-cap
Then(/^the user is redirected from sign-in page$/u, async () => {
    try {
        logger.info(`Validating redirection from sign-in page...`);
        const formElement = await element.all(by.css(`form[action="/session"]`));
        await browser.sleep(2000);
        expect(formElement).to.have.length(0);
    } catch (e) {
        logger.error(e.message);
        throw new Error(e)
    }
});

// eslint-disable-next-line new-cap
Then(/^an error message of invalid credentials appears$/u, async () => {
    try {
        const text = await LoginPage.getErrorMessage();
        logger.info(`Checking the error message text...`);
        expect(text).to.be.deep.equal("Incorrect username or password.");
    } catch (e) {
        logger.error(e.message);
    }
})
