class Album {
  constructor(name, date, tracks, artist) {
    this.albumName = name;
    this.releaseDate = date;
    this.trackList = tracks || [];
    this.artist = artist;
  }

  assignArtist(artist) {
    this.artist = artist;
  }

  addTracks(tracks) {
    tracks.forEach(element => {
      this.trackList.push(element);
    });
  }
}

  module.exports = {
    Album
}