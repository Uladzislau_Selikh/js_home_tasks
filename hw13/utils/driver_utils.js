const {defaultTimeoutMs} = require("./constants");
const {browser} = require("protractor");
const EC = browser.ExpectedConditions;

class DriverUtils {
    static clickElement = async element => {
        await browser.wait(EC.elementToBeClickable(element), defaultTimeoutMs);
        await element.click();
    };

    static getElementInnerText = async element => {
        await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);
        return await element.getText();
    };

    static enterText = async (element, text) => {
        await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);
        await element.sendKeys(text);
    };

    static scrollToElement = async element => {
        await browser.actions().mouseMove(element).perform();
    };
};

module.exports = DriverUtils;