class Library {
    constructor() {
        this.tracks = [];
        this.albums = [];
        this.artists = [];
    }

    add(...items) {
        for (let i = 0; i < items.length; i++) {
            try {
                if (Array.isArray(items[i])) {
                    items[i].forEach((element, index) => {
                        try {
                            if (element.trackName) {
                                if (this.tracks.indexOf(element) === -1) this.tracks.push(element)
                                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
                            } else if (element.albumName) {
                                if (this.albums.indexOf(element) === -1) this.albums.push(element)
                                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
                            } else if (element.artistName) {
                                if (this.artists.indexOf(element) === -1) this.artists.push(element)
                                else throw new Error(`The element ${index + 1} of the array ${i + 1} is already in the Library!`);
                            } else throw new Error(`Please enter valid and existing object name for element ${index + 1} of the array ${i + 1}!`);
                        } catch (error) {
                            console.log(error.message);
                        }
                    });
                } else if (items[i].trackName) {
                    if (this.tracks.indexOf(items[i]) === -1) this.tracks.push(items[i])
                    else throw new Error(`The element ${i + 1} is already in the Library!`);
                } else if (items[i].albumName) {
                    if (this.albums.indexOf(items[i]) === -1) this.albums.push(items[i])
                    else throw new Error(`The element ${i + 1} is already in the Library!`);
                } else if (items[i].artistName) {
                    if (this.artists.indexOf(items[i]) === -1) this.artists.push(items[i])
                    else throw new Error(`The element ${i + 1} is already in the Library!`);
                } else throw new Error('Please enter valid and existing object name!');
            } catch (error) {
                console.log(error.message);
            }
        }
    }

    delete(...items) {
        for (let i = 0; i < items.length; i++) {
            let itemIndex;
            try {
                if (items[i].trackName && this.tracks.indexOf(items[i]) !== -1) {
                    itemIndex = this.tracks.indexOf(items[i])
                    this.tracks.splice(itemIndex, 1);
                } else if (items[i].albumName && this.albums.indexOf(items[i]) !== -1) {
                    itemIndex = this.albums.indexOf(items[i])
                    this.albums.splice(itemIndex, 1);
                } else if (items[i].artistName && this.artists.indexOf(items[i]) !== -1) {
                    itemIndex = this.artists.indexOf(items[i])
                    this.artists.splice(itemIndex, 1);
                } else throw new Error(`The Library doesn't contain item ${i + 1} you have passed! There is nothing to delete!`);
            } catch (error) {
                console.log(error.message);
            }
        }
    }

    update(item, prop, newValue) {
        let itemIndex;
        try {
            if (item.trackName && this.tracks.indexOf(item) !== -1) {
                itemIndex = this.tracks.indexOf(item);
                this.tracks[itemIndex][prop] = newValue;
            } else if (item.albumName && this.albums.indexOf(item) !== -1) {
                itemIndex = this.albums.indexOf(item);
                this.albums[itemIndex][prop] = newValue;
            } else if (item.artistName && this.artists.indexOf(item) !== -1) {
                itemIndex = this.artists.indexOf(item);
                this.artists[itemIndex][prop] = newValue;
            } else throw new Error(`The Library doesn't contain item you have passed! There is nothing to update!`);
        } catch (error) {
            console.log(error.message);
        }
    }

    display() {
        let result = {
            tracks: [],
            albums: [],
            artists: [],
        }

        for (let prop in this) {
            if (result[prop]) {
            this[prop].map(item => {
                result[prop].push(item.artistName ? item.artistName : item.trackName ? item.trackName : item.albumName);
            });
        }
        }

        console.log(result);
    }
}

module.exports = {
    Library
}