function rabbitsCount(months) {
    let rabbits = 200;
    for (let i = 1; i <= months; i++) {
        if (i % 2 === 0) {
        rabbits *= 0.5;
        } else if (rabbits > 100) {
        rabbits *= 0.8;
    }
    rabbits += 20;
    console.log(rabbits);
}
}
module.exports = { rabbitsCount }