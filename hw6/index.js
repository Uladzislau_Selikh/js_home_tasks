const Library = require('./Library').Library;
const Artist = require('./Artist').Artist;
const Album = require('./Album').Album;
const Track = require('./Track').Track;
const loading = require('./loading');

const Godzilla = new Track('Godzilla', 5, true);
const Darkness = new Track('Darkness', 5, true);
const MusicToBeMurderedBy = new Album('Music To Be Murdered By', new Date('05/11/2020'), [Darkness]);
const Eminem = new Artist('Eminem', new Date('12/11/1994'), [Darkness]);
const TillLindemann = new Artist('Till Lindemann', new Date('12/11/1976'));

Godzilla.assignArtist(Eminem);
Godzilla.assisnAlbum(MusicToBeMurderedBy);

Darkness.assignArtist(Eminem);
Darkness.assisnAlbum(MusicToBeMurderedBy);

MusicToBeMurderedBy.assignArtist(Eminem);
MusicToBeMurderedBy.addTracks([Godzilla]);

const LoseYourself = new Track('Lose Yourself', 5, true);
const EightMile = new Album('Eight Mile', new Date('05/11/1995'));

LoseYourself.assignArtist(Eminem);
LoseYourself.assisnAlbum(EightMile);

Eminem.addTracks([LoseYourself, Godzilla]);
Eminem.addAlbums([EightMile, MusicToBeMurderedBy]);
EightMile.assignArtist(Eminem);
EightMile.addTracks([LoseYourself]);

const newLibrary = new Library();

newLibrary.add(LoseYourself, Darkness, EightMile, MusicToBeMurderedBy, Eminem, Godzilla, TillLindemann);

console.log(`======Search Through Library Task======`);
console.log('\n' + `===Search Results For "Eminem", "Eight", "", "em", "Godjirra" and "B"===`);
newLibrary.search('Eminem');
newLibrary.search('Eight');
newLibrary.search('');
newLibrary.search('em');
newLibrary.search('Godjirra');
newLibrary.search('B');

setTimeout(() => console.log('\n======Loading Something Task======'), 500);
loading(2).then(() => process.stdout.write(`Loading is completed!`));