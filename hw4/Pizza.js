class Pizza {

    constructor(size) {
        this.toppings = [];
        this.sizes = size.name;
        this.cost = size.cost;
        this.calories = size.calories;
    }

    static sizes = {
        small: {name: 'small', cost: 50, calories: 20},
        big: {name: 'big', cost: 100, calories: 40}
    }

    static toppings = {
        salami: {name: 'salami', cost: 15, calories: 20},
        cheese: {name: 'cheese', cost: 10, calories: 10},
        meat: {name: 'meat', cost: 20, calories: 15},
        salad: {name: 'salad', cost: 10, calories: 5}
    }
  
    addToppings(item) {
        this.toppings.push(item.name);
        this.cost += item.cost;
        this.calories += item.calories;
    }

    removeToppings(item) {
        try {
        if (this.toppings.indexOf(item.name) !== -1) {
            this.toppings.splice(this.toppings.indexOf(item.name), 1);
            this.cost -= item.cost;
            this.calories -= item.calories;
        } else throw new Error(`Your pizza doesn't contain the topping ${item.name}!`);
    } catch(error) {
        console.log(error.message);
    }
    }

    switchSize(aimSize) {
        try {
            if (aimSize.name !== this.sizes) {
                this.cost += aimSize.cost - PIZZA.sizes[this.sizes].cost;
                this.calories += aimSize.calories - PIZZA.sizes[this.sizes].calories;
                this.sizes = aimSize.name;
                console.log(`Your pizza is ${this.sizes} now!`);
            } else {
                throw new Error(`Your pizza is already of ${aimSize.name} size!`);
            }
        }
        catch(error) {
            console.log(error.message);
        }
    }

    get isVegan() {
        console.log((this.toppings.indexOf('meat') === -1 || this.toppings.indexOf('salami') !== -1)? true : false);
    }

    get costAndCalories() {
        console.log(`The total cost of your pizza is ${this.cost} BYN!\nThe total calories content is ${this.calories} calories!`);
    }

  }

module.exports = {
    Pizza
}