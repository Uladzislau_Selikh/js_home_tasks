let Car = (function () {
    return {
        engine: 'Ford',
        fuelConsumpPer100: 10,
        tankCapacity: 7,
        fuelLevel: 5,
        timesOfRefuel: 0,
  
        refuelCar: function (station, fuel) {
            return station.refuel.call(this, fuel);
        },
        
        passDistance: function (stations, distance) {
  
            if (Object.keys(stations).length < 4 || Object.keys(stations).length > 8) {
                console.log(`Sorry, but the number of stations of ${Object.keys(stations).length} is invalid.`);
                return;
            }
  
            let Mogilev = new GasolineStation();
            stations.push({ station: Mogilev, fuelAmount: 0, distanceFromStart: distance });  //add destination point to the stations map
  
            let passedDistance = 0;  //the variable to display to the car owner after passing the gasoline station
            let fuelConsumed;
            let message = [];  //messages to display to the car owner
            let exactKmPassed = 0;  //the variable is used only when there is not enough fuel left
            let currentStation; // to avoid writing stations[i] each time
  
  
            for (let i = 0; i < stations.length; i++) {
  
                currentStation = stations[i];
                fuelConsumed = (currentStation.distanceFromStart - passedDistance) / 100 * this.fuelConsumpPer100;
  
                if (fuelConsumed <= this.fuelLevel) {
  
                    this.timesOfRefuel++;
                    this.fuelLevel -= fuelConsumed;
                    this.refuelCar(currentStation.station, currentStation.fuelAmount);
  
                    passedDistance = currentStation.distanceFromStart;
  
                    message.push((currentStation.station !== Mogilev) ? `\nStation ${this.timesOfRefuel}:\nYou have passed ${passedDistance} km.\nCurrent fuel level is ${this.fuelLevel} litre(s).` :
  
                        `\nYou have reached the destination point.\nThe distance passed is ${passedDistance} km. \nCurrent fuel level is ${this.fuelLevel} litre(s).`);
  
                }
  
                else {
  
                    exactKmPassed = passedDistance + this.fuelLevel * this.fuelConsumpPer100;
  
                    message.push(`\nNot enough fuel!\nThe nearest gasoline station is in ${currentStation.distanceFromStart - exactKmPassed} km to reach.\nDistance to pass to the destination point is ${distance - exactKmPassed} km.`);
  
                    break;
  
                }
  
            }
  
            message.forEach(item => console.log(item));
  
        }
  
    }
  })();
  
  function GasolineStation() {
    this.refuel = function (gasoline) {
        this.fuelLevel = (this.fuelLevel + gasoline < this.tankCapacity) ? this.fuelLevel + gasoline : this.tankCapacity;
        return this.fuelLevel;
    }
  }
  
  module.exports = {
    Car,
    GasolineStation
  }