const students = [
    {
        name: 'Vasya Pupkin',
        age: 17,
        sex: 'Male',
        isMarried: false,
        city: 'Mogilev',
    },
    {
        name: 'Zoya Petrovna',
        age: 23,
        sex: 'Female',
        isMarried: true,
        city: 'Mogilev',
    },
    {
        name: 'Petr Ivanov',
        age: 30,
        sex: 'Male',
        isMarried: true,
        city: 'Minsk',
    },
    {
        name: 'Vitali Ivanov',
        age: 19,
        sex: 'Male',
        isMarried: false,
        city: 'Vitebsk',
    },
    {
        name: 'Lavrenti Sakalov',
        age: 20,
        sex: 'Male',
        isMarried: true,
        city: 'Brest',
    },
    {
        name: 'Olga Sakalova',
        age: 23,
        sex: 'Female',
        isMarried: true,
        city: 'Grodno',
    },
]

function citiesMatched(city) {
for (let i = 0; i < students.length; i++) {
    if (students[i].city === city) {
    console.log(students[i]);
    }
}
}

function lessOrSameAge(age) {
    for (let i = 0; i < students.length; i++) {
        if (students[i].age <= age) {
        console.log(students[i]);
        }
    }
    }

function sortByName() {
    let nameArr = students.map(item => item.name);
    let sortedArr = nameArr.sort();
    sortedArr.forEach((item) => {
        console.log(item.substr(0, item.indexOf(' ')));
        });
    }

function filterByMarriedStatus() {
        let newArr = students.filter(item => item.sex === "Female" && item.isMarried === true);
        newArr.forEach(item => console.log(item));
        }

function ageAndSexMatched(age, sex) {
    for (let i = 0; i < students.length; i++) {
        if (students[i].age === age && students[i].sex === sex) {
        console.log(students[i]);
        }
    }
    }

function addNewStudent(newStudent) {
    let newArr = [];
    students.forEach((item) => {
        newArr.push(item);
    });
    newArr.unshift(newStudent);
    newArr.forEach(item => console.log(item));
    }
    
function uniqueCities() {
    let cityArr = students.map((item) => item.city);
    let filteredArr = cityArr.filter((item, index) => {
        return cityArr.indexOf(item) === index && cityArr.lastIndexOf(item) === index;
    });
    filteredArr.forEach(item => console.log(item));
    }

module.exports = {
    citiesMatched,
    lessOrSameAge,
    sortByName,
    filterByMarriedStatus,
    ageAndSexMatched,
    addNewStudent,
    uniqueCities
}