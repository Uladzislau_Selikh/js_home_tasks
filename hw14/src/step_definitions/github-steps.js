const { element, by, browser } = require("protractor");
const { expect } = require("chai");
const { generateAlphabeticString } = require("../support/driver_utils");
const LoginPage = require("../pages/login_page");
const { Then, setDefaultTimeout } = require(`@cucumber/cucumber`);
const { validLogin, validPassword, baseUrl } = require("../support/constants");
const WelcomePage = require("../pages/home_page");

setDefaultTimeout(240 * 1000);

// eslint-disable-next-line new-cap
Then(/^open web page (.+)$/u, async (webAddress) => {
    await browser.get(webAddress);
});

// eslint-disable-next-line new-cap
Then(/^scroll to navigation links$/u, async () => {
    await WelcomePage.scrollToNavigationLinks();
});

// eslint-disable-next-line new-cap
Then(/^scroll to header$/u, async () => {
    await WelcomePage.scrollToHeader();
});

// eslint-disable-next-line new-cap
Then(/^the text of the link (.+) contains (.+)$/u, async (number, linkText) => {
    await WelcomePage.getNavigationLinks()
        .get(number - 1)
        .getText()
        .then(text => {
            expect(text).to.be.deep.equal(linkText);
        })
});

// eslint-disable-next-line new-cap
Then(/^click on Sign In button$/u, async () => {
    await WelcomePage.getSignInButton().click();
});

// eslint-disable-next-line new-cap
Then(/^the user is redirected to sign-in page$/u, async () => {
    await WelcomePage.getCurrentUrl
        .then((item) => {
            expect(item).to.contain(`${baseUrl}/login`);
        });
    await browser.sleep(2000);
});

// eslint-disable-next-line new-cap
Then(/^click on Forgot password link$/u, async () => {
    await LoginPage.clickForgotPasswordLink();
    await browser.sleep(2000);
});

// eslint-disable-next-line new-cap
Then(/^the user is on the password reset page$/u, async () => {
    await LoginPage.getCurrentUrl.then(item => {
        expect(item).to.contain(`${baseUrl}/password_reset`);
    })
});

// eslint-disable-next-line new-cap
Then(/^log in with invalid username of (.+) symbols length$/u, async (length) => {
    const invalidLogin = generateAlphabeticString(length);
    await LoginPage.performLogin(invalidLogin, validPassword);
    await browser.sleep(2000);
});

// eslint-disable-next-line new-cap
Then(/^log in with invalid password of (.+) symbols length$/u, async (length) => {
    const invalidPassword = generateAlphabeticString(length);
    await LoginPage.performLogin(validLogin, invalidPassword);
    await browser.sleep(2000);
});

// eslint-disable-next-line new-cap
Then(/^log in with valid credentials$/u, async () => {
    await LoginPage.performLogin(validLogin, validPassword);
    await browser.sleep(2000);
});

// eslint-disable-next-line new-cap
Then(/^the user is redirected from sign-in page$/u, async () => {
    const formElement = await element.all(by.css(`form[action="/session"]`));
    await browser.sleep(2000);
    expect(formElement).to.have.length(0);
});

// eslint-disable-next-line new-cap
Then(/^an error message of invalid credentials appears$/u, async () => {
    const text = await LoginPage.getErrorMessage();
    expect(text).to.be.deep.equal("Incorrect username or password.");
})
